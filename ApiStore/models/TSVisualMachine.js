'use strict';
var webConfig = require('../helper/config');
var client = new require('../helper/client');

class TSVisualMachine {

  static get _proxyServerUrl() {
    return webConfig.proxyServerUrl;
  }

  static get _ip() {
    return webConfig.ip;
  }

  static activate(token) {
    return new Promise(function (resolve, reject) {
      client.get(TSVisualMachine._ip + ':8129/startDevice?accessToken=' + token + '&vendorThingID=0999E-W40-C-180').then(function (response) {
        console.log(response);
        resolve(response);

      }).catch(function (error) {
        console.log('activate device error', error);
      })
    })
  }

  static getDevices(token) {
    return new Promise(function (resolve, reject) {
      client.get(TSVisualMachine._ip + ':8129/listDevices?accessToken=' + token).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        console.log(getDevices.name + ' error', error);
      })
    })
  }

  static getApiKey(token) {
    var _that = this;
    return new Promise(function (resolve, reject) {
      client.get(_that._proxyServerUrl + '/3rdparty/apiKey', {
        headers: {
          'Authorization': 'Bearer ' + token
        }
      }).then(function (response) {
        resolve(response);
      }).catch(function (error) {
        console.log('get api key error', error);
      })
    })
  }
}

module.exports = TSVisualMachine;