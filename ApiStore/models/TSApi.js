'use strict';
var webConfig = require('../helper/config');
var client = new require('../helper/client');


class TSApi {
  constructor(data) {
    Object.assign(this, data);
  }

  static get _baseUrl() {
    return webConfig.proxyServerUrl + '/apis';
  }

  static findBySearch(name) {
    var _that = this;
    return new Promise(function (resolve, reject) {
      client.get(_that._baseUrl + '/query/' + encodeURIComponent(name)).then(function (response) {
        let apis = [];
        for (let data of response) {
          apis.push(new TSApi(data));
        }
        resolve(apis);
      }).catch(function (error) {
        console.log('error');
        throw (error);
      });
    })
  }

  static findByTagId(tagID) {
    var _that = this;
    return new Promise(function (resolve, reject) {
      client.get(_that._baseUrl + '/tagquery/' + tagID).then(function (response) {
        let apis = [];
        for (let data of response) {
          apis.push(new TSApi(data));
        }
        resolve(apis);
      }).catch(function (error) {
        console.log('error');
        throw (error);
      });
    })
  }

  static findByTagIDs(ids) {
    var _that = this;
    return new Promise(function (resolve, reject) {
      client.get(_that._baseUrl + '/tagsquery/' + ids).then(function (response) {
        let apis = [];
        for (let data of response) {
          apis.push(new TSApi(data));
        }
        resolve(apis);
      }).catch(function (error) {
        console.log('error', error);
        throw (error);
      });
    })
  }

  static findVersionsByTagIDs(ids) {
    return new Promise(function (resolve, reject) {
      client.get(TSApi._baseUrl + '/tags-query/' + ids + "/latest-version").then(function (response) {
        let apis = [];
        for (let data of response) {
          apis.push(data);
        }
        resolve(apis);
      }).catch(function (error) {
        console.log('error', error);
        throw (error);
      })
    })
  }

  static findAll() {
    var _that = this;
    return new Promise(function (resolve, reject) {
      client.get(_that._baseUrl).then(function (response) {
        let apis = [];

        for (let data of response) {
          apis.push(new TSApi(data));
        }

        resolve(apis);
      }).catch(function (error) {
        reject(error);
      });
    });
  }

  static getApiVersionByVersionID(apiID, versionID) {
    let _that = this;
    return new Promise(function (resolve, reject) {
      client.get(_that._baseUrl + '/' + apiID + '/wholeApi/version-id/' + versionID).then(function (response) {
        let api = response;
        resolve(api);
      }).catch(function (error) {
        console.log(' error', error);
      });
    })
  }

  static getLatestApiVersionByID(id) {
    let _that = this;
    return new Promise(function (resolve, reject) {
      client.get(_that._baseUrl + '/' + id + '/latest-version').then(function (response) {
        let api = response;
        resolve(api);
      }).catch(function (error) {
        console.log(getLatestApiVersionByID.name + ' error', error);
      });
    })
  }

  static getTagsByApiID(id) {
    var _that = this;
    return new Promise(function (resolve, reject) {
      client.get(_that._baseUrl + "/" + id + "/tags").then(function (response) {
        let tags = [];
        // console.log("get tags by api id response", response);
        for (let data of response) {
          tags.push(data);
        }
        resolve(tags);
      }).catch(function (error) {
        console.log(getTagsByApiID.name + ' error', error);
      });
    });
  }
}

module.exports = TSApi;