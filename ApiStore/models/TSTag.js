"use strict";
var client = new require('../helper/client');
var webConfig = require('../helper/config');
// var TSTag = require('../helper/dbhelper/sequelize-models/api_tag')(SequelizeHelper.instance, Sequelize);



class TSTag {
  constructor(data) {
    Object.assign(this, data);
  }

  static get _baseUrl() {
    return webConfig.proxyServerUrl + '/tags';
  }

  static findMainTags() {
    var _that = this;
    return new Promise(function (resolve, reject) {
      client.get(_that._baseUrl + '/topLevel').then(function (response) {
        let tags = [];
        let $defers = [];

        for (let data of response) {
          let tag = new TSTag(data)
          tags.push(tag);
          $defers.push(tag.findSubTags());
        }

        Promise.all($defers).then(function () {
          resolve(tags);
        }).catch(function (error) {
          console.log(findMainTags.name + ' error',error);
        });

      }).catch(function (error) {
        reject(error);
      });
    })
  }

  findSubTags() {
    let _that = this;
    return new Promise(function (resolve, reject) {
      client.get(TSTag._baseUrl + '/topLevel/' + _that.id).then(function (response) {
        let tags = [];

        for (let data of response) {
          tags.push(new TSTag(data));
        }

        _that.subTags = tags;

        resolve(_that);
      }).catch(function (error) {
        reject(error);
      });
    });
  }


  static getByID(id) {
    let _that = this;
    return new Promise(function (resolve, reject) {

      client.get(_that._baseUrl + '/entitys/' + id).then(function (response) {
        let api = new TSTag(response);
        resolve(api);
      }).catch(function (error) {
        console.log('get tag by id error', error);
      });
    })
  }
  static getByApiID(id) {

  }
}

module.exports = TSTag;