'use strict';
var webConfig = require('../helper/config');
var client = new require('../helper/client');

class TSApiExampleCode {
  constructor(data) {
    Object.assign(this, data);
  }

  static get _baseUrl() {
    return webConfig.proxyServerUrl + '/versions';
  }

  static getByVersionID(versionID) {
    return new Promise(function (resolve, reject) {
      client.get(TSApiExampleCode._baseUrl + "/" + versionID + "/examplecodes").then(function (response) {
        console.log(TSApiExampleCode._baseUrl + "/" + versionID + "/examplecodes");
        let exampleCodes = [];

        for (let data of response) {
          exampleCodes.push(data);
        }

        resolve(exampleCodes);
      }).catch(function (error) {
        console.log(getByVersionID.name + ' error', error);
      });
    })
  }

  static getResponseByVersionID(versionID) {
    var _that = this;
    return new Promise(function (resolve, reject) {
      client.get(_that._baseUrl + "/" + versionID + "/exampleresponse").then(function (response) {
        console.log(_that._baseUrl + "/" + versionID + "/exampleresponse");
        let exampleResponses = [];

        for (let data of response) {
          exampleResponses.push(data);
        }

        resolve(exampleResponses);
      }).catch(function (error) {
        console.log(getResponseByVersionID.name + ' error', error);
      });
    })
  }
}

module.exports = TSApiExampleCode;