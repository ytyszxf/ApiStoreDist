'use strict';
var webConfig = require('../helper/config');
var client = new require('../helper/client');

class TSUser {
  constructor(data) {
    Object.assign(this, data);
  }

  static get _baseUrl() {
    return webConfig.proxyServerUrl;
  }

  static get _kiiCloud() {
    return webConfig.kiiCloud;
  }

  static get _ip() {
    return webConfig.ip;
  }

  static getBeehiveToken() {
    console.log('start getting beehive token');
    return new Promise(function (resolve, reject) {
      client.post(TSUser._ip + ':8080/beehive-portal/api/oauth2/login', {
        headers: {
          'Content-Type': 'application/json'
        },
        data: {
          'userName': 'apistore',
          'password': 'apistore',
        }
      }).then(function (response) {
        if (response.accessToken) {
          resolve(response.accessToken);
        } else {
          resolve(null);
        }
      }).catch(function (error) {
        console.log('get beehive token error:', error);
      })
    })
  };

  static getApiKey(token) {
    return new Promise(function (resolve, reject) {
      client.get(TSUser._baseUrl + '/3rdparty/apiKey', {
        headers: {
          'Authorization': 'Bearer ' + token
        }
      }).then(function (response) {
        // if (Buffer.isBuffer(response)) {
        //   response = response.toString('utf8');
        // }
        let apiKey = response;
        resolve(apiKey);
      }).catch(function (error) {
        console.log(error);
      });
    })
  }
  static getUser(token) {
    let _that = this;
    return new Promise(function (resolve, reject) {
      client.get(_that._kiiCloud.endpoint + '/apps/' + _that._kiiCloud.appID + '/users/me', {
        headers: {
          'Authorization': 'Bearer ' + token,
          'X-Kii-AppID': _that._kiiCloud.appID,
          'X-Kii-AppKey': _that._kiiCloud.appKey
        }
      }).then(function (response) {
        if (Buffer.isBuffer(response)) {
          response = JSON.parse(response.toString('utf8'));
        }
        let user = response;
        resolve(user);
      }).catch(function (error) {
        console.log(error);
      });
    })
  }
}

module.exports = TSUser;