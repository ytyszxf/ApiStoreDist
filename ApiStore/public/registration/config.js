(function () {
  'use strict';

  window.appConfig = {
    DEMO: {
      cloudApiUrl: 'https://api-jp.kii.com/api',
      baseUrl: 'http://54.223.39.102:8081/3rdpartyapiserver',
      apiUrl: 'http://54.223.39.102:8081/3rdpartyapiserver/api',
      kiiAppID: '39eeccb8',
      kiiAppKey: 'f94a471dc4a25ffeb392a87bb1dd135c',
      beehiveApiUrl: 'http://54.223.39.102:8080/beehive-portal/api'
    },
    LOCAL: {
      cloudApiUrl: 'https://api-development-beehivecn3.internal.kii.com/api',
      baseUrl: 'http://localhost:9091/3rdpartyapiserver',
      apiUrl: 'http://localhost:9091/3rdpartyapiserver/api',
      kiiAppID: 'a0370808',
      kiiAppKey: '4e126a56657a1c410a0717decc6bc69c',
      beehiveApiUrl: 'http://114.215.196.178:8080/beehive-portal/api'
    },
    DEV: {
      cloudApiUrl: 'https://api-development-beehivecn3.internal.kii.com/api',
      baseUrl: 'http://114.215.196.178:8081/3rdpartyapiserver',
      apiUrl: 'http://114.215.196.178:8081/3rdpartyapiserver/api',
      kiiAppID: 'a0370808',
      kiiAppKey: '4e126a56657a1c410a0717decc6bc69c',
      beehiveApiUrl: 'http://114.215.196.178:8080/beehive-portal/api'
    },
    QA: {
      cloudApiUrl: 'https://api-development-beehivecn3.internal.kii.com/api',
      baseUrl: 'http://114.215.178.24:8081/3rdpartyapiserver',
      apiUrl: 'http://api.openibplatform.com',
      kiiAppID: 'a07e56dc',
      kiiAppKey: 'f600091915b2856a33351faf3df306ce',
      beehiveApiUrl: 'http://114.215.178.24:8080/beehive-portal/api'
    },
    PRODUCTION: {
      cloudApiUrl: 'https://api-beehivecn4.kii.com/api',
      baseUrl: 'http://120.77.83.143:8081/3rdpartyapiserver',
      apiUrl: 'http://120.77.83.143:8081/3rdpartyapiserver/api/',
      kiiAppID: 'wfenb44dvlyi',
      kiiAppKey: '6f5b2d230cae45a4ae36f856dedf8554',
      beehiveApiUrl: 'http://120.77.83.143:8080/beehive-portal/api'
    },
    ENV: 'DEV',
    LANGUAGE: 'cn',
    LOCALES: {
      cn: {},
      en: {}
    }
  };

  var cn = {
    userLogin: '用户注册',
    rememberMe: '记住我',
    login: '登录',
    registration: '用户注册',
    register: '注册'
  };

  var en = {
    userLogin: 'User Login',
    rememberMe: 'Remember Me',
    login: 'Login',
    registration: 'Registration',
    register: 'Register'
  };

  window.appConfig.LOCALES.cn = cn;
  window.appConfig.LOCALES.en = en;

})();