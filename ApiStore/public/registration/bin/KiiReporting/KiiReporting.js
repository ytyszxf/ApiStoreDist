webpackJsonp([1,2],{

/***/ 298:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var root_1 = __webpack_require__(121);
	var toSubscriber_1 = __webpack_require__(308);
	var symbol_observable_1 = __webpack_require__(302);
	/**
	 * A representation of any set of values over any amount of time. This the most basic building block
	 * of RxJS.
	 *
	 * @class Observable<T>
	 */
	var Observable = (function () {
	    /**
	     * @constructor
	     * @param {Function} subscribe the function that is  called when the Observable is
	     * initially subscribed to. This function is given a Subscriber, to which new values
	     * can be `next`ed, or an `error` method can be called to raise an error, or
	     * `complete` can be called to notify of a successful completion.
	     */
	    function Observable(subscribe) {
	        this._isScalar = false;
	        if (subscribe) {
	            this._subscribe = subscribe;
	        }
	    }
	    /**
	     * Creates a new Observable, with this Observable as the source, and the passed
	     * operator defined as the new observable's operator.
	     * @method lift
	     * @param {Operator} operator the operator defining the operation to take on the observable
	     * @return {Observable} a new observable with the Operator applied
	     */
	    Observable.prototype.lift = function (operator) {
	        var observable = new Observable();
	        observable.source = this;
	        observable.operator = operator;
	        return observable;
	    };
	    /**
	     * Registers handlers for handling emitted values, error and completions from the observable, and
	     *  executes the observable's subscriber function, which will take action to set up the underlying data stream
	     * @method subscribe
	     * @param {PartialObserver|Function} observerOrNext (optional) either an observer defining all functions to be called,
	     *  or the first of three possible handlers, which is the handler for each value emitted from the observable.
	     * @param {Function} error (optional) a handler for a terminal event resulting from an error. If no error handler is provided,
	     *  the error will be thrown as unhandled
	     * @param {Function} complete (optional) a handler for a terminal event resulting from successful completion.
	     * @return {ISubscription} a subscription reference to the registered handlers
	     */
	    Observable.prototype.subscribe = function (observerOrNext, error, complete) {
	        var operator = this.operator;
	        var sink = toSubscriber_1.toSubscriber(observerOrNext, error, complete);
	        if (operator) {
	            operator.call(sink, this);
	        }
	        else {
	            sink.add(this._subscribe(sink));
	        }
	        if (sink.syncErrorThrowable) {
	            sink.syncErrorThrowable = false;
	            if (sink.syncErrorThrown) {
	                throw sink.syncErrorValue;
	            }
	        }
	        return sink;
	    };
	    /**
	     * @method forEach
	     * @param {Function} next a handler for each value emitted by the observable
	     * @param {PromiseConstructor} [PromiseCtor] a constructor function used to instantiate the Promise
	     * @return {Promise} a promise that either resolves on observable completion or
	     *  rejects with the handled error
	     */
	    Observable.prototype.forEach = function (next, PromiseCtor) {
	        var _this = this;
	        if (!PromiseCtor) {
	            if (root_1.root.Rx && root_1.root.Rx.config && root_1.root.Rx.config.Promise) {
	                PromiseCtor = root_1.root.Rx.config.Promise;
	            }
	            else if (root_1.root.Promise) {
	                PromiseCtor = root_1.root.Promise;
	            }
	        }
	        if (!PromiseCtor) {
	            throw new Error('no Promise impl found');
	        }
	        return new PromiseCtor(function (resolve, reject) {
	            var subscription = _this.subscribe(function (value) {
	                if (subscription) {
	                    // if there is a subscription, then we can surmise
	                    // the next handling is asynchronous. Any errors thrown
	                    // need to be rejected explicitly and unsubscribe must be
	                    // called manually
	                    try {
	                        next(value);
	                    }
	                    catch (err) {
	                        reject(err);
	                        subscription.unsubscribe();
	                    }
	                }
	                else {
	                    // if there is NO subscription, then we're getting a nexted
	                    // value synchronously during subscription. We can just call it.
	                    // If it errors, Observable's `subscribe` imple will ensure the
	                    // unsubscription logic is called, then synchronously rethrow the error.
	                    // After that, Promise will trap the error and send it
	                    // down the rejection path.
	                    next(value);
	                }
	            }, reject, resolve);
	        });
	    };
	    Observable.prototype._subscribe = function (subscriber) {
	        return this.source.subscribe(subscriber);
	    };
	    /**
	     * An interop point defined by the es7-observable spec https://github.com/zenparsing/es-observable
	     * @method Symbol.observable
	     * @return {Observable} this instance of the observable
	     */
	    Observable.prototype[symbol_observable_1.default] = function () {
	        return this;
	    };
	    // HACK: Since TypeScript inherits static properties too, we have to
	    // fight against TypeScript here so Subject can have a different static create signature
	    /**
	     * Creates a new cold Observable by calling the Observable constructor
	     * @static true
	     * @owner Observable
	     * @method create
	     * @param {Function} subscribe? the subscriber function to be passed to the Observable constructor
	     * @return {Observable} a new cold observable
	     */
	    Observable.create = function (subscribe) {
	        return new Observable(subscribe);
	    };
	    return Observable;
	}());
	exports.Observable = Observable;


/***/ },

/***/ 299:
/***/ function(module, exports) {

	"use strict";
	exports.empty = {
	    isUnsubscribed: true,
	    next: function (value) { },
	    error: function (err) { throw err; },
	    complete: function () { }
	};


/***/ },

/***/ 300:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var isFunction_1 = __webpack_require__(120);
	var Subscription_1 = __webpack_require__(301);
	var Observer_1 = __webpack_require__(299);
	var rxSubscriber_1 = __webpack_require__(118);
	/**
	 * Implements the {@link Observer} interface and extends the
	 * {@link Subscription} class. While the {@link Observer} is the public API for
	 * consuming the values of an {@link Observable}, all Observers get converted to
	 * a Subscriber, in order to provide Subscription-like capabilities such as
	 * `unsubscribe`. Subscriber is a common type in RxJS, and crucial for
	 * implementing operators, but it is rarely used as a public API.
	 *
	 * @class Subscriber<T>
	 */
	var Subscriber = (function (_super) {
	    __extends(Subscriber, _super);
	    /**
	     * @param {Observer|function(value: T): void} [destinationOrNext] A partially
	     * defined Observer or a `next` callback function.
	     * @param {function(e: ?any): void} [error] The `error` callback of an
	     * Observer.
	     * @param {function(): void} [complete] The `complete` callback of an
	     * Observer.
	     */
	    function Subscriber(destinationOrNext, error, complete) {
	        _super.call(this);
	        this.syncErrorValue = null;
	        this.syncErrorThrown = false;
	        this.syncErrorThrowable = false;
	        this.isStopped = false;
	        switch (arguments.length) {
	            case 0:
	                this.destination = Observer_1.empty;
	                break;
	            case 1:
	                if (!destinationOrNext) {
	                    this.destination = Observer_1.empty;
	                    break;
	                }
	                if (typeof destinationOrNext === 'object') {
	                    if (destinationOrNext instanceof Subscriber) {
	                        this.destination = destinationOrNext;
	                        this.destination.add(this);
	                    }
	                    else {
	                        this.syncErrorThrowable = true;
	                        this.destination = new SafeSubscriber(this, destinationOrNext);
	                    }
	                    break;
	                }
	            default:
	                this.syncErrorThrowable = true;
	                this.destination = new SafeSubscriber(this, destinationOrNext, error, complete);
	                break;
	        }
	    }
	    Subscriber.prototype[rxSubscriber_1.$$rxSubscriber] = function () { return this; };
	    /**
	     * A static factory for a Subscriber, given a (potentially partial) definition
	     * of an Observer.
	     * @param {function(x: ?T): void} [next] The `next` callback of an Observer.
	     * @param {function(e: ?any): void} [error] The `error` callback of an
	     * Observer.
	     * @param {function(): void} [complete] The `complete` callback of an
	     * Observer.
	     * @return {Subscriber<T>} A Subscriber wrapping the (partially defined)
	     * Observer represented by the given arguments.
	     */
	    Subscriber.create = function (next, error, complete) {
	        var subscriber = new Subscriber(next, error, complete);
	        subscriber.syncErrorThrowable = false;
	        return subscriber;
	    };
	    /**
	     * The {@link Observer} callback to receive notifications of type `next` from
	     * the Observable, with a value. The Observable may call this method 0 or more
	     * times.
	     * @param {T} [value] The `next` value.
	     * @return {void}
	     */
	    Subscriber.prototype.next = function (value) {
	        if (!this.isStopped) {
	            this._next(value);
	        }
	    };
	    /**
	     * The {@link Observer} callback to receive notifications of type `error` from
	     * the Observable, with an attached {@link Error}. Notifies the Observer that
	     * the Observable has experienced an error condition.
	     * @param {any} [err] The `error` exception.
	     * @return {void}
	     */
	    Subscriber.prototype.error = function (err) {
	        if (!this.isStopped) {
	            this.isStopped = true;
	            this._error(err);
	        }
	    };
	    /**
	     * The {@link Observer} callback to receive a valueless notification of type
	     * `complete` from the Observable. Notifies the Observer that the Observable
	     * has finished sending push-based notifications.
	     * @return {void}
	     */
	    Subscriber.prototype.complete = function () {
	        if (!this.isStopped) {
	            this.isStopped = true;
	            this._complete();
	        }
	    };
	    Subscriber.prototype.unsubscribe = function () {
	        if (this.isUnsubscribed) {
	            return;
	        }
	        this.isStopped = true;
	        _super.prototype.unsubscribe.call(this);
	    };
	    Subscriber.prototype._next = function (value) {
	        this.destination.next(value);
	    };
	    Subscriber.prototype._error = function (err) {
	        this.destination.error(err);
	        this.unsubscribe();
	    };
	    Subscriber.prototype._complete = function () {
	        this.destination.complete();
	        this.unsubscribe();
	    };
	    return Subscriber;
	}(Subscription_1.Subscription));
	exports.Subscriber = Subscriber;
	/**
	 * We need this JSDoc comment for affecting ESDoc.
	 * @ignore
	 * @extends {Ignored}
	 */
	var SafeSubscriber = (function (_super) {
	    __extends(SafeSubscriber, _super);
	    function SafeSubscriber(_parent, observerOrNext, error, complete) {
	        _super.call(this);
	        this._parent = _parent;
	        var next;
	        var context = this;
	        if (isFunction_1.isFunction(observerOrNext)) {
	            next = observerOrNext;
	        }
	        else if (observerOrNext) {
	            context = observerOrNext;
	            next = observerOrNext.next;
	            error = observerOrNext.error;
	            complete = observerOrNext.complete;
	            if (isFunction_1.isFunction(context.unsubscribe)) {
	                this.add(context.unsubscribe.bind(context));
	            }
	            context.unsubscribe = this.unsubscribe.bind(this);
	        }
	        this._context = context;
	        this._next = next;
	        this._error = error;
	        this._complete = complete;
	    }
	    SafeSubscriber.prototype.next = function (value) {
	        if (!this.isStopped && this._next) {
	            var _parent = this._parent;
	            if (!_parent.syncErrorThrowable) {
	                this.__tryOrUnsub(this._next, value);
	            }
	            else if (this.__tryOrSetError(_parent, this._next, value)) {
	                this.unsubscribe();
	            }
	        }
	    };
	    SafeSubscriber.prototype.error = function (err) {
	        if (!this.isStopped) {
	            var _parent = this._parent;
	            if (this._error) {
	                if (!_parent.syncErrorThrowable) {
	                    this.__tryOrUnsub(this._error, err);
	                    this.unsubscribe();
	                }
	                else {
	                    this.__tryOrSetError(_parent, this._error, err);
	                    this.unsubscribe();
	                }
	            }
	            else if (!_parent.syncErrorThrowable) {
	                this.unsubscribe();
	                throw err;
	            }
	            else {
	                _parent.syncErrorValue = err;
	                _parent.syncErrorThrown = true;
	                this.unsubscribe();
	            }
	        }
	    };
	    SafeSubscriber.prototype.complete = function () {
	        if (!this.isStopped) {
	            var _parent = this._parent;
	            if (this._complete) {
	                if (!_parent.syncErrorThrowable) {
	                    this.__tryOrUnsub(this._complete);
	                    this.unsubscribe();
	                }
	                else {
	                    this.__tryOrSetError(_parent, this._complete);
	                    this.unsubscribe();
	                }
	            }
	            else {
	                this.unsubscribe();
	            }
	        }
	    };
	    SafeSubscriber.prototype.__tryOrUnsub = function (fn, value) {
	        try {
	            fn.call(this._context, value);
	        }
	        catch (err) {
	            this.unsubscribe();
	            throw err;
	        }
	    };
	    SafeSubscriber.prototype.__tryOrSetError = function (parent, fn, value) {
	        try {
	            fn.call(this._context, value);
	        }
	        catch (err) {
	            parent.syncErrorValue = err;
	            parent.syncErrorThrown = true;
	            return true;
	        }
	        return false;
	    };
	    SafeSubscriber.prototype._unsubscribe = function () {
	        var _parent = this._parent;
	        this._context = null;
	        this._parent = null;
	        _parent.unsubscribe();
	    };
	    return SafeSubscriber;
	}(Subscriber));


/***/ },

/***/ 301:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var isArray_1 = __webpack_require__(306);
	var isObject_1 = __webpack_require__(307);
	var isFunction_1 = __webpack_require__(120);
	var tryCatch_1 = __webpack_require__(309);
	var errorObject_1 = __webpack_require__(119);
	var UnsubscriptionError_1 = __webpack_require__(305);
	/**
	 * Represents a disposable resource, such as the execution of an Observable. A
	 * Subscription has one important method, `unsubscribe`, that takes no argument
	 * and just disposes the resource held by the subscription.
	 *
	 * Additionally, subscriptions may be grouped together through the `add()`
	 * method, which will attach a child Subscription to the current Subscription.
	 * When a Subscription is unsubscribed, all its children (and its grandchildren)
	 * will be unsubscribed as well.
	 *
	 * @class Subscription
	 */
	var Subscription = (function () {
	    /**
	     * @param {function(): void} [unsubscribe] A function describing how to
	     * perform the disposal of resources when the `unsubscribe` method is called.
	     */
	    function Subscription(unsubscribe) {
	        /**
	         * A flag to indicate whether this Subscription has already been unsubscribed.
	         * @type {boolean}
	         */
	        this.isUnsubscribed = false;
	        if (unsubscribe) {
	            this._unsubscribe = unsubscribe;
	        }
	    }
	    /**
	     * Disposes the resources held by the subscription. May, for instance, cancel
	     * an ongoing Observable execution or cancel any other type of work that
	     * started when the Subscription was created.
	     * @return {void}
	     */
	    Subscription.prototype.unsubscribe = function () {
	        var hasErrors = false;
	        var errors;
	        if (this.isUnsubscribed) {
	            return;
	        }
	        this.isUnsubscribed = true;
	        var _a = this, _unsubscribe = _a._unsubscribe, _subscriptions = _a._subscriptions;
	        this._subscriptions = null;
	        if (isFunction_1.isFunction(_unsubscribe)) {
	            var trial = tryCatch_1.tryCatch(_unsubscribe).call(this);
	            if (trial === errorObject_1.errorObject) {
	                hasErrors = true;
	                (errors = errors || []).push(errorObject_1.errorObject.e);
	            }
	        }
	        if (isArray_1.isArray(_subscriptions)) {
	            var index = -1;
	            var len = _subscriptions.length;
	            while (++index < len) {
	                var sub = _subscriptions[index];
	                if (isObject_1.isObject(sub)) {
	                    var trial = tryCatch_1.tryCatch(sub.unsubscribe).call(sub);
	                    if (trial === errorObject_1.errorObject) {
	                        hasErrors = true;
	                        errors = errors || [];
	                        var err = errorObject_1.errorObject.e;
	                        if (err instanceof UnsubscriptionError_1.UnsubscriptionError) {
	                            errors = errors.concat(err.errors);
	                        }
	                        else {
	                            errors.push(err);
	                        }
	                    }
	                }
	            }
	        }
	        if (hasErrors) {
	            throw new UnsubscriptionError_1.UnsubscriptionError(errors);
	        }
	    };
	    /**
	     * Adds a tear down to be called during the unsubscribe() of this
	     * Subscription.
	     *
	     * If the tear down being added is a subscription that is already
	     * unsubscribed, is the same reference `add` is being called on, or is
	     * `Subscription.EMPTY`, it will not be added.
	     *
	     * If this subscription is already in an `isUnsubscribed` state, the passed
	     * tear down logic will be executed immediately.
	     *
	     * @param {TeardownLogic} teardown The additional logic to execute on
	     * teardown.
	     * @return {Subscription} Returns the Subscription used or created to be
	     * added to the inner subscriptions list. This Subscription can be used with
	     * `remove()` to remove the passed teardown logic from the inner subscriptions
	     * list.
	     */
	    Subscription.prototype.add = function (teardown) {
	        if (!teardown || (teardown === this) || (teardown === Subscription.EMPTY)) {
	            return;
	        }
	        var sub = teardown;
	        switch (typeof teardown) {
	            case 'function':
	                sub = new Subscription(teardown);
	            case 'object':
	                if (sub.isUnsubscribed || typeof sub.unsubscribe !== 'function') {
	                    break;
	                }
	                else if (this.isUnsubscribed) {
	                    sub.unsubscribe();
	                }
	                else {
	                    (this._subscriptions || (this._subscriptions = [])).push(sub);
	                }
	                break;
	            default:
	                throw new Error('Unrecognized teardown ' + teardown + ' added to Subscription.');
	        }
	        return sub;
	    };
	    /**
	     * Removes a Subscription from the internal list of subscriptions that will
	     * unsubscribe during the unsubscribe process of this Subscription.
	     * @param {Subscription} subscription The subscription to remove.
	     * @return {void}
	     */
	    Subscription.prototype.remove = function (subscription) {
	        // HACK: This might be redundant because of the logic in `add()`
	        if (subscription == null || (subscription === this) || (subscription === Subscription.EMPTY)) {
	            return;
	        }
	        var subscriptions = this._subscriptions;
	        if (subscriptions) {
	            var subscriptionIndex = subscriptions.indexOf(subscription);
	            if (subscriptionIndex !== -1) {
	                subscriptions.splice(subscriptionIndex, 1);
	            }
	        }
	    };
	    Subscription.EMPTY = (function (empty) {
	        empty.isUnsubscribed = true;
	        return empty;
	    }(new Subscription()));
	    return Subscription;
	}());
	exports.Subscription = Subscription;


/***/ },

/***/ 302:
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(303);


/***/ },

/***/ 303:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	
	var _ponyfill = __webpack_require__(304);
	
	var _ponyfill2 = _interopRequireDefault(_ponyfill);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var root = undefined; /* global window */
	
	if (typeof global !== 'undefined') {
		root = global;
	} else if (typeof window !== 'undefined') {
		root = window;
	}
	
	var result = (0, _ponyfill2.default)(root);
	exports.default = result;
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },

/***/ 304:
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.default = symbolObservablePonyfill;
	function symbolObservablePonyfill(root) {
		var result;
		var _Symbol = root.Symbol;
	
		if (typeof _Symbol === 'function') {
			if (_Symbol.observable) {
				result = _Symbol.observable;
			} else {
				result = _Symbol('observable');
				_Symbol.observable = result;
			}
		} else {
			result = '@@observable';
		}
	
		return result;
	};

/***/ },

/***/ 118:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var root_1 = __webpack_require__(121);
	var Symbol = root_1.root.Symbol;
	exports.$$rxSubscriber = (typeof Symbol === 'function' && typeof Symbol.for === 'function') ?
	    Symbol.for('rxSubscriber') : '@@rxSubscriber';


/***/ },

/***/ 305:
/***/ function(module, exports) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	/**
	 * An error thrown when one or more errors have occurred during the
	 * `unsubscribe` of a {@link Subscription}.
	 */
	var UnsubscriptionError = (function (_super) {
	    __extends(UnsubscriptionError, _super);
	    function UnsubscriptionError(errors) {
	        _super.call(this);
	        this.errors = errors;
	        var err = Error.call(this, errors ?
	            errors.length + " errors occurred during unsubscription:\n  " + errors.map(function (err, i) { return ((i + 1) + ") " + err.toString()); }).join('\n  ') : '');
	        this.name = err.name = 'UnsubscriptionError';
	        this.stack = err.stack;
	        this.message = err.message;
	    }
	    return UnsubscriptionError;
	}(Error));
	exports.UnsubscriptionError = UnsubscriptionError;


/***/ },

/***/ 119:
/***/ function(module, exports) {

	"use strict";
	// typeof any so that it we don't have to cast when comparing a result to the error object
	exports.errorObject = { e: {} };


/***/ },

/***/ 306:
/***/ function(module, exports) {

	"use strict";
	exports.isArray = Array.isArray || (function (x) { return x && typeof x.length === 'number'; });


/***/ },

/***/ 120:
/***/ function(module, exports) {

	"use strict";
	function isFunction(x) {
	    return typeof x === 'function';
	}
	exports.isFunction = isFunction;


/***/ },

/***/ 307:
/***/ function(module, exports) {

	"use strict";
	function isObject(x) {
	    return x != null && typeof x === 'object';
	}
	exports.isObject = isObject;


/***/ },

/***/ 121:
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(module, global) {"use strict";
	var objectTypes = {
	    'boolean': false,
	    'function': true,
	    'object': true,
	    'number': false,
	    'string': false,
	    'undefined': false
	};
	exports.root = (objectTypes[typeof self] && self) || (objectTypes[typeof window] && window);
	/* tslint:disable:no-unused-variable */
	var freeExports = objectTypes[typeof exports] && exports && !exports.nodeType && exports;
	var freeModule = objectTypes[typeof module] && module && !module.nodeType && module;
	var freeGlobal = objectTypes[typeof global] && global;
	if (freeGlobal && (freeGlobal.global === freeGlobal || freeGlobal.window === freeGlobal)) {
	    exports.root = freeGlobal;
	}
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(311)(module), (function() { return this; }())))

/***/ },

/***/ 308:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var Subscriber_1 = __webpack_require__(300);
	var rxSubscriber_1 = __webpack_require__(118);
	function toSubscriber(nextOrObserver, error, complete) {
	    if (nextOrObserver) {
	        if (nextOrObserver instanceof Subscriber_1.Subscriber) {
	            return nextOrObserver;
	        }
	        if (nextOrObserver[rxSubscriber_1.$$rxSubscriber]) {
	            return nextOrObserver[rxSubscriber_1.$$rxSubscriber]();
	        }
	    }
	    if (!nextOrObserver && !error && !complete) {
	        return new Subscriber_1.Subscriber();
	    }
	    return new Subscriber_1.Subscriber(nextOrObserver, error, complete);
	}
	exports.toSubscriber = toSubscriber;


/***/ },

/***/ 309:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var errorObject_1 = __webpack_require__(119);
	var tryCatchTarget;
	function tryCatcher() {
	    try {
	        return tryCatchTarget.apply(this, arguments);
	    }
	    catch (e) {
	        errorObject_1.errorObject.e = e;
	        return errorObject_1.errorObject;
	    }
	}
	function tryCatch(fn) {
	    tryCatchTarget = fn;
	    return tryCatcher;
	}
	exports.tryCatch = tryCatch;
	;


/***/ },

/***/ 311:
/***/ function(module, exports) {

	module.exports = function(module) {
		if(!module.webpackPolyfill) {
			module.deprecate = function() {};
			module.paths = [];
			// module.parent = undefined by default
			module.children = [];
			module.webpackPolyfill = 1;
		}
		return module;
	}


/***/ },

/***/ 83:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var KiiQuery_1 = __webpack_require__(88);
	var ChartAggregationNode = (function (_super) {
	    __extends(ChartAggregationNode, _super);
	    function ChartAggregationNode(obj) {
	        _super.call(this, obj);
	        _.extend(this, obj);
	        var _that = this;
	        _.each(this.children, function (child, i) {
	            _that.children[i] = new ChartAggregationNode(child);
	        });
	    }
	    return ChartAggregationNode;
	}(KiiQuery_1.AggregationNode));
	exports.ChartAggregationNode = ChartAggregationNode;


/***/ },

/***/ 122:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var ChartAggregationNode_1 = __webpack_require__(83);
	var components_1 = __webpack_require__(136);
	var KiiChartUtils_1 = __webpack_require__(48);
	var KiiQueryConfig_1 = __webpack_require__(43);
	var DOM_WATCH_INTERVAL = 300;
	var KiiChart = (function () {
	    function KiiChart(aggTree, $selector, options, theme) {
	        if (theme === void 0) { theme = 'default'; }
	        this._parent = null;
	        this._domWidth = null;
	        this._domHeight = null;
	        this._$selector = $selector;
	        var initialOptions = {
	            _backButton: {
	                style: {
	                    position: 'absolute',
	                    top: '10px',
	                    left: '10px'
	                },
	                text: '返回'
	            }
	        };
	        this._options = _.extend(initialOptions, KiiQueryConfig_1.KiiQueryConfig.instance.getChartOptions(), options);
	        this._aggTree = new ChartAggregationNode_1.ChartAggregationNode(KiiChartUtils_1.KiiChartUtils.clone(aggTree));
	        if (!this._$selector || !this._$selector.get(0)) {
	            throw new Error('No given container!');
	        }
	        this._init();
	    }
	    KiiChart.prototype._init = function () {
	        var _this = this;
	        this._legend = new components_1.Legend(this);
	        this._seriesGroups = [];
	        this._yAxises = [];
	        this._xAxis = null;
	        this._generateToolTip();
	        this._parent = this._options._parent;
	        this._canvas = $('<div></div>').appendTo(this._$selector);
	        this._canvas
	            .css({
	            'height': '100%',
	            'width': '100%'
	        });
	        this._echartInstance = ECharts.init(this._canvas.get(0), 'default', this._options);
	        if (this._parent) {
	            var _that_1 = this;
	            var $button = $('<button>' + this._options._backButton.text + '</button>').appendTo(this._canvas);
	            var buttonSettings = this._options._backButton;
	            $button.addClass(buttonSettings.cssClass);
	            $button.css(buttonSettings.style);
	            $button.click(function ($event) {
	                _that_1.goBack();
	            });
	        }
	        this._echartInstance.on('click', function (event) {
	            _this._drilldown(event);
	        });
	        this._watchDom();
	    };
	    KiiChart.prototype._hide = function () {
	        this._canvas.hide();
	    };
	    KiiChart.prototype._show = function () {
	        var _this = this;
	        this._canvas.show();
	        setTimeout(function () {
	            _this._resize();
	        });
	    };
	    KiiChart.prototype.updateAggTree = function (aggNode) {
	        this._aggTree = new ChartAggregationNode_1.ChartAggregationNode(KiiChartUtils_1.KiiChartUtils.clone(aggNode));
	    };
	    KiiChart.prototype.refresh = function () {
	        var _this = this;
	        var _that = this;
	        this._options.series = [];
	        if (this._dataChanged) {
	            this._xAxis = null;
	            this._yAxises = [];
	            this._factorySeriesGroups();
	            this._generateXAxis();
	            _.each(this._seriesGroups, function (s) {
	                s.useXAxis(_this._xAxis);
	                _that._generateYAxis(s);
	            });
	        }
	        var seriesGroups = this._seriesGroups;
	        var series = [];
	        var xAxis = this._xAxis ? this._xAxis.getProperties() : {};
	        var yAxises = [];
	        var dataZooms = [];
	        var tooltip = this._toolTip.getProperties();
	        var legend = this._legend;
	        legend.refresh();
	        _.each(this._selectedAggNode.metrics, function (metric) {
	            if (!metric.selected)
	                return;
	            var seriesGroup = _.find(seriesGroups, function (g) { return g.getMetric() == metric; });
	            if (!seriesGroup)
	                return;
	            var seriesSet = seriesGroup.getSeriesSet();
	            series = series.concat(seriesSet);
	            var yAxis = _.find(_that._yAxises, function (yx) {
	                return yx.getGroupID() == (metric.group || 0);
	            });
	            if (!yAxis)
	                return;
	            if (_.findIndex(yAxises, { groupID: yAxis.getGroupID() }) < 0) {
	                yAxises.push(yAxis.getProperties());
	            }
	            _.each(seriesSet, function (s) {
	                s.yAxisIndex = _.findIndex(yAxises, { groupID: yAxis.getGroupID() });
	                return s;
	            });
	        });
	        if (this._seriesGroups.length > 0) {
	            if (this._seriesGroups[0].xAxis) {
	                dataZooms.push(this._seriesGroups[0].xAxis.dataZoom);
	            }
	            _.each(this._yAxises, function (yx, i) {
	                yx.dataZoom.yAxisIndex = i;
	                dataZooms.push(yx.dataZoom);
	            });
	            _.extend(this._options, {
	                xAxis: this._seriesGroups[0] instanceof components_1.PieSeriesGroup ? null : xAxis,
	                yAxis: yAxises,
	                series: series,
	                dataZoom: dataZooms,
	                tooltip: tooltip,
	                legend: legend.getProperties()
	            });
	        }
	        this._echartInstance.setOption(this._getEchartOptions());
	    };
	    KiiChart.prototype._getEchartOptions = function () {
	        var _options = {};
	        _.each(this._options, function (option, key) {
	            if (key.indexOf('_') == 0)
	                return;
	            _options[key] = option;
	        });
	        return _options;
	    };
	    KiiChart.prototype._retrievalAggTree = function (func) {
	        var _currentNode = this._aggTree;
	        var _targetNode = this.getSelectedAggNode();
	        var _fullTrace = _targetNode.trace.concat([]).splice(_currentNode.trace.length);
	        if (!_currentNode)
	            return;
	        func(_currentNode);
	        _.each(_fullTrace, function (_trace) {
	            _currentNode = _currentNode.children[_trace];
	            func(_currentNode);
	        });
	    };
	    KiiChart.prototype._watchDom = function () {
	        var $dom = this._$selector;
	        var _that = this;
	        this._domWatcher = setInterval(function () {
	            if (_that._domWidth != $dom.width() || _that._domHeight != $dom.height()) {
	                _that._domWidth = $dom.width();
	                _that._domHeight = $dom.height();
	                _that._resize();
	            }
	        }, DOM_WATCH_INTERVAL);
	    };
	    KiiChart.prototype._resize = function () {
	        this._echartInstance.resize();
	    };
	    KiiChart.prototype.goBack = function () {
	        this._parent._show();
	        this._destroy();
	    };
	    KiiChart.prototype._drilldown = function (event) {
	        if (this._seriesGroups[0] instanceof components_1.PieSeriesGroup) {
	            this.drilldownByCategory(event);
	            return;
	        }
	        switch (this._aggTree.type) {
	            case "category":
	                this.drilldownByCategory(event);
	                break;
	            default:
	                this.dirlldownByValue(event);
	                break;
	        }
	    };
	    KiiChart.prototype.dirlldownByValue = function (event) {
	        var data = this.getData();
	        var chart = KiiChart.createChart(this._aggTree.children[this.aggTrace[0]], this.getSelectedAggNode(), this._$selector, { _parent: this });
	        chart.selectAggNode(this.getSelectedAggNode());
	        chart.setData(data[this._aggTree.fieldName][this._aggTree.keySet[event.dataIndex]]);
	        chart.refresh();
	        this._hide();
	    };
	    KiiChart.prototype.drilldownByCategory = function (event) {
	        var dimensions = event.data.dimensions;
	        var keys = Object.keys(dimensions);
	        if (keys.length > this.aggTrace.length)
	            return;
	        var data = this.getData();
	        var aggNode = this._aggTree;
	        var _that = this;
	        var i = 0;
	        _.each(dimensions, function (dimension) {
	            data = _.find(data[aggNode.fieldName], function (doc, key) {
	                return key == dimension;
	            });
	            aggNode = aggNode.children[_that.aggTrace[i++]];
	        });
	        var chart = KiiChart.createChart(aggNode, this.getSelectedAggNode(), this._$selector, { _parent: this });
	        chart.selectAggNode(this.getSelectedAggNode());
	        chart.setData(data);
	        chart.refresh();
	        this._hide();
	    };
	    KiiChart.prototype.setData = function (data) {
	        this._originData = KiiChartUtils_1.KiiChartUtils.clone(data);
	        this._data = KiiChartUtils_1.KiiChartUtils.clone(this._originData);
	        this._dataChanged = true;
	    };
	    KiiChart.prototype.getData = function () {
	        return this._originData;
	    };
	    KiiChart.prototype.destroy = function () {
	        this._destroy();
	    };
	    KiiChart.prototype._destroy = function () {
	        this._echartInstance.dispose();
	        this._canvas.remove();
	        clearInterval(this._domWatcher);
	    };
	    KiiChart.prototype.getSelectedAggNode = function () {
	        return this._selectedAggNode;
	    };
	    KiiChart.prototype.selectAggNode = function (node) {
	        this._selectedAggNode = this._aggTree;
	        var _trace = node.trace.concat([]);
	        _trace.splice(0, this._aggTree.trace.length);
	        for (var i = _trace.length; i > 0; i--) {
	            this._selectedAggNode = this._selectedAggNode.children[_trace.shift()];
	        }
	        if (!_.find(this._selectedAggNode.metrics, (function (metric) { return metric.selected; }))) {
	            this._selectedAggNode.metrics[0].selected = true;
	        }
	        this.aggTrace = node.trace.concat([]).splice(this._aggTree.trace.length);
	    };
	    KiiChart.prototype._factorySeriesGroups = function () {
	        this._dataChanged = false;
	        var seriesGroups = [];
	        var _chart = this;
	        this._seriesGroups = seriesGroups;
	        var selectedNode = this._selectedAggNode;
	        var metrics = selectedNode.getSelectedMetrics();
	        var chartType = this._aggTree.chart || selectedNode.metrics[0].chart;
	        _.each(metrics, function (metric) {
	            var seriesGroup;
	            switch (chartType) {
	                case 'pie':
	                    seriesGroup = new components_1.PieSeriesGroup(_chart, metric);
	                    break;
	                case 'line':
	                    seriesGroup = new components_1.LineSeriesGroup(_chart, metric);
	                    break;
	                case 'bar':
	                    seriesGroup = new components_1.BarSeriesGroup(_chart, metric);
	                    break;
	                default:
	                    seriesGroup = new components_1.LineSeriesGroup(_chart, metric);
	                    break;
	            }
	            seriesGroups.push(seriesGroup);
	        });
	        this._seriesGroups = seriesGroups;
	    };
	    KiiChart.prototype.getSeriesGroups = function () {
	        return this._seriesGroups;
	    };
	    KiiChart.prototype.getAggTree = function () {
	        return this._aggTree;
	    };
	    KiiChart.createChart = function (aggregations, level, $selector, options) {
	        var chart = new KiiChart(aggregations, $selector, options);
	        chart.selectAggNode(level);
	        return chart;
	    };
	    KiiChart.prototype.generateSeriesData = function (type, metric) {
	        switch (type) {
	            case 'category':
	                return this._getSeriesDatasetByCategory(metric);
	            default:
	                return this._getSeriesDatasetByValue(metric);
	        }
	    };
	    KiiChart.prototype.getContainer = function () {
	        return this._$selector;
	    };
	    KiiChart.prototype._generateXAxis = function () {
	        var aggRoot = this.getAggTree();
	        var _options = {};
	        if (aggRoot.type == 'category') {
	            var options = {
	                name: aggRoot.displayName,
	                type: 'category',
	                splitLine: {
	                    show: false
	                },
	                data: []
	            };
	            this._xAxis = new components_1.XAxis(options);
	        }
	        else {
	            var _data = aggRoot.keySet;
	            var options = {
	                name: aggRoot.displayName,
	                type: 'category',
	                splitLine: {
	                    show: false
	                },
	                data: _data
	            };
	            this._xAxis = new components_1.XAxis(options);
	        }
	        this._xAxis.setDataType(aggRoot.type);
	    };
	    KiiChart.prototype._generateYAxis = function (seriesGroup) {
	        if (seriesGroup instanceof components_1.PieSeriesGroup)
	            return;
	        var metric = seriesGroup.getMetric();
	        var yAxis = _.find(this._yAxises, function (yAxis) { return yAxis.getGroupID() == (metric.group || 0); });
	        if (yAxis) {
	            seriesGroup.useYAxis(yAxis);
	            return;
	        }
	        var _that = this;
	        var options = {
	            name: metric.displayName
	        };
	        yAxis = new components_1.YAxis(options);
	        yAxis.setGroupID(metric.group || 0);
	        seriesGroup.useYAxis(yAxis);
	        this._yAxises.push(yAxis);
	    };
	    KiiChart.prototype._generateToolTip = function () {
	        switch (this._aggTree.type) {
	            case 'value':
	            case 'time':
	                this._toolTip = new components_1.ToolTip({ trigger: 'axis' });
	                break;
	            default:
	                this._toolTip = new components_1.ToolTip({ trigger: 'item' });
	                break;
	        }
	        if (this._aggTree.chart == 'pie') {
	            this._toolTip.formatter = "{b}<br/> {c} ({d}%)";
	        }
	    };
	    KiiChart.prototype._getSeriesDatasetByValue = function (myMetric) {
	        var _this = this;
	        var _aggs = [];
	        var _data = this._data;
	        var _rootNode = this._aggTree;
	        var _aggNode = this.getSelectedAggNode();
	        var _xData = this._aggTree.keySet;
	        var _yData = [];
	        var currentNode = this.getSelectedAggNode();
	        if (this._aggTree.trace.length == currentNode.trace.length) {
	            _yData = [retrieval(currentNode, currentNode, _data[currentNode.fieldName], null, {})];
	        }
	        else {
	            var _childNode_1 = this._aggTree.children[currentNode.trace[this._aggTree.trace.length]];
	            _.each(this._aggTree.keySet, function (key) {
	                var _dimension = {};
	                _dimension[_this._aggTree.trace.join('-')] = key;
	                var _result = retrieval(_childNode_1, currentNode, _data[_this._aggTree.fieldName][key][_childNode_1.fieldName], null, _dimension);
	                _yData = _yData.concat(_result);
	            });
	            _yData = _.groupBy(_yData, 'name');
	        }
	        function retrieval(rootNode, targetNode, data, name, dimensions) {
	            var trace = targetNode.trace.concat([]).splice(rootNode.trace.length);
	            if (trace.length) {
	                var results_1 = [];
	                _.each(rootNode.keySet, function (key) {
	                    _.each(rootNode.children, function (child) {
	                        var _name = (name ? (name + ' ') : '') + key;
	                        var _dimension = {};
	                        _dimension[rootNode.trace.join('-')] = key;
	                        _.extend(_dimension, dimensions);
	                        var _result = retrieval(child, targetNode, data[key][child.fieldName], _name, _dimension);
	                        results_1 = results_1.concat(_result);
	                    });
	                });
	                return results_1;
	            }
	            else {
	                var result_1 = [];
	                _.each(targetNode.keySet, function (key) {
	                    var _dimension = {};
	                    _dimension[targetNode.trace.join('-')] = key;
	                    _.extend(_dimension, dimensions);
	                    var _data = {
	                        name: (name ? (name + ' ') : '') + key,
	                        dimensions: _dimension
	                    };
	                    _data[myMetric.fieldName] = { "name": key, value: data[key][myMetric.fieldName] };
	                    result_1.push(_data);
	                });
	                return result_1;
	            }
	        }
	        return { x: _xData, y: _yData };
	    };
	    KiiChart.prototype._getSeriesDatasetByCategory = function (myMetric) {
	        var _aggs = [];
	        var _data = this._data;
	        var _rootNode = this._aggTree;
	        var _aggNode = this.getSelectedAggNode();
	        var _categoryData = [];
	        this._retrievalAggTree(function (aggNode) {
	            _categoryData.push(retrieval(_rootNode, aggNode, _data[_rootNode.fieldName], {}));
	        });
	        function retrieval(rootNode, targetNode, data, dimensions) {
	            var trace = targetNode.trace.concat([]).splice(rootNode.trace.length);
	            if (trace.length) {
	                var results_2 = [];
	                _.each(rootNode.keySet, function (key) {
	                    _.each(rootNode.children, function (child) {
	                        var _dimension = {};
	                        _dimension[rootNode.trace.join('-')] = key;
	                        _.extend(_dimension, dimensions);
	                        var _result = retrieval(child, targetNode, data[key][child.fieldName], _dimension);
	                        results_2 = results_2.concat(_result);
	                    });
	                });
	                return results_2;
	            }
	            else {
	                var result_2 = [];
	                _.each(targetNode.keySet, function (key) {
	                    var _dimension = {};
	                    _dimension[targetNode.trace.join('-')] = key;
	                    _.extend(_dimension, dimensions);
	                    var _data = {
	                        name: key,
	                        dimensions: _dimension,
	                        value: data[key][myMetric.fieldName]
	                    };
	                    result_2.push(_data);
	                });
	                return result_2;
	            }
	        }
	        return { x: [], y: _categoryData };
	    };
	    return KiiChart;
	}());
	exports.KiiChart = KiiChart;


/***/ },

/***/ 48:
/***/ function(module, exports) {

	"use strict";
	exports.KiiChartUtils = {
	    extractSeries: function (options) {
	        return options.series;
	    },
	    clone: function (obj) {
	        return JSON.parse(JSON.stringify(obj));
	    },
	    dateParse: function (date) {
	        function pad(num) {
	            var str = num + '';
	            return str.length < 2 ? '0' + str : str;
	        }
	        return date.getFullYear() + '/' +
	            pad(date.getMonth() + 1) + '/' +
	            pad(date.getDate()) + ' ' +
	            pad(date.getHours()) + ':' +
	            pad(date.getMinutes());
	    },
	    guid: function () {
	        function s4() {
	            return Math.floor((1 + Math.random()) * 0x10000)
	                .toString(16)
	                .substring(1);
	        }
	        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
	            s4() + '-' + s4() + s4() + s4();
	    }
	};


/***/ },

/***/ 84:
/***/ function(module, exports) {

	"use strict";
	var DataZoom = (function () {
	    function DataZoom(options) {
	        this.show = true;
	        _.extend(this, options);
	    }
	    DataZoom.prototype.update = function (options) {
	        _.extend(this, options);
	    };
	    return DataZoom;
	}());
	exports.DataZoom = DataZoom;


/***/ },

/***/ 123:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var _axis_1 = __webpack_require__(59);
	var KiiChartUtils_1 = __webpack_require__(48);
	var XAxis = (function (_super) {
	    __extends(XAxis, _super);
	    function XAxis(options) {
	        var _options = {
	            type: 'category',
	            splitLine: {
	                show: false
	            },
	            silent: true
	        };
	        _.extend(_options, options);
	        _super.call(this, options);
	        this.dataZoom.xAxisIndex = 0;
	    }
	    XAxis.prototype.setDataType = function (type) {
	        this._dataType = type;
	    };
	    XAxis.prototype.refresh = function () {
	    };
	    XAxis.prototype._getProperties = function () {
	        var properties = {
	            data: this.data
	        };
	        var formatter = function (d) {
	            return KiiChartUtils_1.KiiChartUtils.dateParse(new Date(d));
	        };
	        properties.data = _.map(properties.data, formatter);
	        if (this._dataType == 'time') {
	            _.extend(properties, {
	                axisLabel: {}
	            });
	        }
	        return properties;
	    };
	    return XAxis;
	}(_axis_1._axis));
	exports.XAxis = XAxis;


/***/ },

/***/ 124:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var _axis_1 = __webpack_require__(59);
	var SeriesGroup_1 = __webpack_require__(85);
	;
	var YAxis = (function (_super) {
	    __extends(YAxis, _super);
	    function YAxis(options) {
	        var _this = this;
	        var _options = {
	            minInterval: 1,
	            type: 'value',
	            splitLine: {
	                show: false
	            },
	            axisLabel: {
	                formatter: function (value, index) {
	                    return value / _this._scaleTimes;
	                }
	            },
	            boundaryGap: ['10%', '10%'],
	            silent: true,
	            scale: true
	        };
	        _.extend(_options, options);
	        _super.call(this, _options);
	        this.dataZoom.show = false;
	        this._dataset = this._dataset || [];
	        this._seriesGroups = [];
	    }
	    YAxis.prototype.setGroupID = function (groupID) {
	        this._groupID = groupID;
	    };
	    YAxis.prototype.getGroupID = function () {
	        return this._groupID;
	    };
	    YAxis.prototype.refresh = function () {
	        this._scale();
	    };
	    YAxis.prototype.addSeriesGroup = function (s) {
	        if (this._seriesGroups.indexOf(s) < 0)
	            this._seriesGroups.push(s);
	    };
	    YAxis.prototype._getProperties = function () {
	        var _that = this;
	        var formatter = function (value) {
	            return (value / _that._scaleTimes) | 0;
	        };
	        var scale = this.scale;
	        if (this._seriesGroups.length > 0 && this._seriesGroups[0] instanceof SeriesGroup_1.BarSeriesGroup) {
	            scale = false;
	        }
	        return {
	            name: this.name + '(单位：' + this._scaleTimes + ')',
	            groupID: this.getGroupID(),
	            axisLabel: {
	                formatter: formatter
	            },
	            scale: scale
	        };
	    };
	    YAxis.prototype._scale = function () {
	        var _that = this;
	        var _max = 0;
	        var _indexes = [];
	        this._scaleTimes = 1;
	        var _dataset = [];
	        _.each(this._seriesGroups, function (s, i) {
	            if (!s.isShown())
	                return;
	            var tmp = _.pluck(s.getSeriesSet(), 'data');
	            _.each(tmp, function (da) {
	                if (da.length < 1)
	                    return;
	                if (_.isNumber(da[0])) {
	                    _dataset = _dataset.concat(da);
	                }
	                else {
	                    _dataset = _dataset.concat(_.pluck(da, 'value'));
	                }
	            });
	        });
	        _dataset = _.map(_dataset, function (da) { return Math.abs(da); });
	        _max = _dataset.length > 0 ? _.max(_dataset) : 0;
	        if (Math.abs(_max) == Infinity) {
	            return;
	        }
	        while (_max >= 1000) {
	            _max /= 10;
	            this._scaleTimes *= 10;
	        }
	        while (_max < 10 && _max != 0) {
	            _max *= 10;
	            this._scaleTimes /= 10;
	        }
	    };
	    return YAxis;
	}(_axis_1._axis));
	exports.YAxis = YAxis;


/***/ },

/***/ 59:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var DataZoom_1 = __webpack_require__(84);
	var AxisPropertyNames = ['name', 'axisLabel', 'type', 'scale',
	    'boundrayGap', 'silent', 'splitLine', 'data'];
	var _axis = (function () {
	    function _axis(options) {
	        _.extend(this, options);
	        var dataZoomOptions = {
	            show: true,
	            realtime: true,
	            start: 0,
	            end: 100,
	        };
	        this.dataZoom = new DataZoom_1.DataZoom(dataZoomOptions);
	    }
	    _axis.prototype.update = function (options) {
	        _.extend(this, options);
	    };
	    _axis.prototype.getProperties = function () {
	        var _this = this;
	        var options = {};
	        _.each(AxisPropertyNames, function (key) {
	            if (_this.hasOwnProperty(key)) {
	                options[key] = _this[key];
	            }
	        });
	        _.extend(options, this._getProperties());
	        return options;
	    };
	    return _axis;
	}());
	exports._axis = _axis;


/***/ },

/***/ 125:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	function __export(m) {
	    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
	}
	__export(__webpack_require__(123));
	__export(__webpack_require__(124));
	__export(__webpack_require__(84));
	var _axis_1 = __webpack_require__(59);
	exports.IAxisProperties = _axis_1.IAxisProperties;


/***/ },

/***/ 126:
/***/ function(module, exports) {

	"use strict";
	var LegendOptionsFields = ['data'];
	var Legend = (function () {
	    function Legend(chart, options) {
	        this._chart = chart;
	        _.extend(this, options);
	    }
	    Legend.prototype.refresh = function () {
	        var _this = this;
	        this.data = [];
	        _.each(this._chart.getSeriesGroups(), function (s) {
	            if (s.isShown()) {
	                _this.data = _this.data.concat(s.names);
	            }
	        });
	    };
	    Legend.prototype.getProperties = function () {
	        var _that = this;
	        var properties = {};
	        _.each(LegendOptionsFields, function (key) {
	            if (_that.hasOwnProperty(key)) {
	                properties[key] = _that[key];
	            }
	        });
	        return properties;
	    };
	    return Legend;
	}());
	exports.Legend = Legend;


/***/ },

/***/ 127:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	function __export(m) {
	    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
	}
	__export(__webpack_require__(126));


/***/ },

/***/ 128:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var SeriesGroup_1 = __webpack_require__(49);
	var BarSeriesGroup = (function (_super) {
	    __extends(BarSeriesGroup, _super);
	    function BarSeriesGroup(_chart, metric) {
	        _super.call(this, _chart, metric);
	    }
	    BarSeriesGroup.prototype._init = function () {
	        var _this = this;
	        var _data = null;
	        var _that = this;
	        var aggNode = this._chart.getSelectedAggNode();
	        var aggRoot = this._chart.getAggTree();
	        _data = this._chart.generateSeriesData(aggRoot.type, this._metric);
	        this._data = _data;
	        if (aggRoot.type == 'category') {
	            this._seriesSet = _.map(_data.y[0], function (series, index) {
	                return {
	                    name: series.name,
	                    type: 'bar',
	                    showSymbol: true,
	                    hoverAnimation: true,
	                    data: [series.value],
	                    groupID: _this._uuid
	                };
	            });
	        }
	        else {
	            this._seriesSet = _.map(_data.y, function (group, key) {
	                var _dimensions = {};
	                if (group.length) {
	                    _.each(group[0].dimensions, function (dimension, aggIndex) {
	                        if (aggIndex == aggRoot.trace.join('-')) {
	                            return;
	                        }
	                        _dimensions[aggIndex] = dimension;
	                    });
	                }
	                return {
	                    name: key,
	                    type: 'bar',
	                    showSymbol: true,
	                    hoverAnimation: true,
	                    data: _.pluck(group, _this._metric.fieldName),
	                    dimensions: _dimensions,
	                    groupID: _this._uuid
	                };
	            });
	        }
	        this.names = _.pluck(this._seriesSet, 'name');
	    };
	    BarSeriesGroup.prototype.useXAxis = function (xAxis) {
	        this.xAxis = xAxis;
	        if (this._chart.getAggTree().type == 'category') {
	            xAxis.dataZoom.show = false;
	        }
	    };
	    return BarSeriesGroup;
	}(SeriesGroup_1.SeriesGroup));
	exports.BarSeriesGroup = BarSeriesGroup;


/***/ },

/***/ 129:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	function __export(m) {
	    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
	}
	__export(__webpack_require__(128));


/***/ },

/***/ 130:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var SeriesGroup_1 = __webpack_require__(49);
	var LineSeriesGroup = (function (_super) {
	    __extends(LineSeriesGroup, _super);
	    function LineSeriesGroup(_chart, metric) {
	        _super.call(this, _chart, metric);
	    }
	    LineSeriesGroup.prototype._init = function () {
	        var _this = this;
	        var _data = null;
	        var _that = this;
	        var aggNode = this._chart.getSelectedAggNode();
	        var aggRoot = this._chart.getAggTree();
	        _data = this._chart.generateSeriesData(aggRoot.type, this._metric);
	        this._data = _data;
	        this._seriesSet = _.map(_data.y, function (group, key) {
	            var _dimensions = {};
	            if (group.length) {
	                _.each(group[0].dimensions, function (dimension, aggIndex) {
	                    if (aggIndex == aggRoot.trace.join('-')) {
	                        return;
	                    }
	                    _dimensions[aggIndex] = dimension;
	                });
	            }
	            return {
	                name: _this._metric.seriesName || key || 0,
	                type: 'line',
	                showSymbol: true,
	                hoverAnimation: true,
	                data: _.pluck(group, _this._metric.fieldName),
	                dimensions: _dimensions,
	                groupID: _this._uuid
	            };
	        });
	        this.names = _.pluck(this._seriesSet, 'name');
	    };
	    return LineSeriesGroup;
	}(SeriesGroup_1.SeriesGroup));
	exports.LineSeriesGroup = LineSeriesGroup;


/***/ },

/***/ 131:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	function __export(m) {
	    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
	}
	__export(__webpack_require__(130));


/***/ },

/***/ 132:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var SeriesGroup_1 = __webpack_require__(49);
	var KiiChartUtils_1 = __webpack_require__(48);
	var PieSeriesGroup = (function (_super) {
	    __extends(PieSeriesGroup, _super);
	    function PieSeriesGroup(_chart, metric) {
	        _super.call(this, _chart, metric);
	    }
	    PieSeriesGroup.prototype._init = function () {
	        var _this = this;
	        var _that = this;
	        var aggRoot = this._chart.getAggTree();
	        var _data = this._chart.generateSeriesData('category', this._metric);
	        this._data = _data;
	        this.names = [];
	        this._seriesSet = _.map(_data.y, function (series, index) {
	            var _t = index;
	            var _aggNode = aggRoot;
	            while (_t--) {
	                _aggNode = _aggNode.children[_this._chart.aggTrace[_t]];
	            }
	            if (_aggNode.type == 'time') {
	                _.each(series, function (s) {
	                    s.name = KiiChartUtils_1.KiiChartUtils.dateParse(new Date(s.name));
	                });
	            }
	            _this.names = _this.names.concat(_.pluck(series, 'name'));
	            return {
	                type: 'pie',
	                showSymbol: true,
	                hoverAnimation: true,
	                data: series,
	                groupID: _this._uuid,
	                radius: _this._generateRadius(index, _data.y.length)
	            };
	        });
	    };
	    PieSeriesGroup.prototype._generateRadius = function (index, length) {
	        var radiusSet = [];
	        switch (length) {
	            case 1:
	                return [
	                    [0, '60%']
	                ][index];
	            case 2:
	                return [
	                    [0, '30%'],
	                    ['40%', '55%']
	                ][index];
	            case 3:
	                return [
	                    [0, '20%'],
	                    ['30%', '45%'],
	                    ['55%', '65%']
	                ][index];
	        }
	    };
	    PieSeriesGroup.prototype.useXAxis = function (xAxis) {
	        this.xAxis = null;
	    };
	    PieSeriesGroup.prototype.useYAxis = function (yAxis) {
	        this.yAxis = null;
	    };
	    return PieSeriesGroup;
	}(SeriesGroup_1.SeriesGroup));
	exports.PieSeriesGroup = PieSeriesGroup;


/***/ },

/***/ 133:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	function __export(m) {
	    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
	}
	__export(__webpack_require__(132));


/***/ },

/***/ 49:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var KiiChartUtils_1 = __webpack_require__(48);
	var SeriesGroup = (function () {
	    function SeriesGroup(_chart, metric) {
	        this._show = false;
	        this._chart = _chart;
	        this._uuid = KiiChartUtils_1.KiiChartUtils.guid();
	        this._metric = metric;
	        if (this._metric.selected) {
	            this._show = true;
	        }
	        this._init();
	    }
	    SeriesGroup.prototype.getID = function () {
	        return this._uuid;
	    };
	    SeriesGroup.prototype.show = function () {
	        if (this._show)
	            return;
	        this._show = true;
	        this._chart.refresh();
	    };
	    SeriesGroup.prototype.hide = function () {
	        if (!this._show)
	            return;
	        this._show = false;
	        this._chart.refresh();
	    };
	    SeriesGroup.prototype.isShown = function () {
	        return this._show;
	    };
	    SeriesGroup.prototype.getSeriesSet = function () {
	        return this._seriesSet;
	    };
	    SeriesGroup.prototype.getMetric = function () {
	        return this._metric;
	    };
	    SeriesGroup.prototype.useXAxis = function (xAxis) {
	        this.xAxis = xAxis;
	    };
	    SeriesGroup.prototype.useYAxis = function (yAxis) {
	        this.yAxis = yAxis;
	        yAxis.addSeriesGroup(this);
	        yAxis.refresh();
	    };
	    return SeriesGroup;
	}());
	exports.SeriesGroup = SeriesGroup;


/***/ },

/***/ 85:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	function __export(m) {
	    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
	}
	__export(__webpack_require__(49));
	__export(__webpack_require__(129));
	__export(__webpack_require__(131));
	__export(__webpack_require__(133));


/***/ },

/***/ 134:
/***/ function(module, exports) {

	"use strict";
	var ToolTip = (function () {
	    function ToolTip(options) {
	        var _options = {
	            axisPointer: {
	                type: 'shadow'
	            }
	        };
	        _.extend(_options, options);
	        _.extend(this, _options);
	    }
	    ToolTip.prototype.getProperties = function () {
	        return {
	            axisPointer: this.axisPointer,
	            trigger: this.trigger,
	            formatter: this.formatter
	        };
	    };
	    return ToolTip;
	}());
	exports.ToolTip = ToolTip;


/***/ },

/***/ 135:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	function __export(m) {
	    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
	}
	__export(__webpack_require__(134));


/***/ },

/***/ 136:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	function __export(m) {
	    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
	}
	__export(__webpack_require__(125));
	__export(__webpack_require__(127));
	__export(__webpack_require__(85));
	__export(__webpack_require__(135));


/***/ },

/***/ 137:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	function __export(m) {
	    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
	}
	__export(__webpack_require__(122));
	__export(__webpack_require__(83));


/***/ },

/***/ 86:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var KiiQueryCompiler_1 = __webpack_require__(139);
	var KiiQueryDataParser_1 = __webpack_require__(87);
	var KiiQueryUtils_1 = __webpack_require__(60);
	var AggregationNode = (function () {
	    function AggregationNode(obj) {
	    }
	    AggregationNode.prototype.getSelectedMetrics = function () {
	        var metrics = [];
	        _.each(this.metrics, function (metric) {
	            if (metric.selected) {
	                metrics.push(metric);
	            }
	        });
	        return metrics;
	    };
	    AggregationNode.prototype.compile = function (queryJSON) {
	        return deepCompile(queryJSON, this);
	        function deepCompile(queryJSON, aggNode) {
	            _.each(aggNode.children, function (_a) {
	                deepCompile(queryJSON[aggNode.fieldName].aggs, _a);
	            });
	            _.each(aggNode.metrics, function (_m) {
	                var metricCompiler = KiiQueryCompiler_1.KiiMetricCompilers[_m.type || 'default']
	                    || KiiQueryCompiler_1.KiiMetricCompilers['default'];
	                metricCompiler(KiiQueryUtils_1.KiiQueryUtils.getAggregations(queryJSON[aggNode.fieldName]), _m);
	            });
	            var _compiler = KiiQueryCompiler_1.KiiAggregationCompilers[aggNode.aggMethod || 'default']
	                || KiiQueryCompiler_1.KiiAggregationCompilers['default'];
	            _compiler(queryJSON, aggNode);
	        }
	    };
	    AggregationNode.prototype.parse = function (doc) {
	        var _that = this;
	        var _parser = KiiQueryDataParser_1.KiiAggDataParsers[this.aggMethod || 'default']
	            || KiiQueryDataParser_1.KiiAggDataParsers['default'];
	        _parser(doc, this);
	        _.each(doc[this.fieldName].buckets, function (bucket, index) {
	            _.each(_that.children, function (child) {
	                child.parse(bucket);
	            });
	            _.each(_that.metrics, function (metric) {
	                bucket[metric.fieldName] = bucket[metric.fieldName] || { value: 0 };
	            });
	        });
	    };
	    AggregationNode.prototype.parseMetrics = function (parentContext) {
	        var _this = this;
	        var _that = this;
	        var index = 0;
	        _.each(this.keySet, function (key) {
	            var _context = new KiiQueryDataParser_1.KiiMetricParserContext({
	                doc: parentContext.doc[_this.fieldName][key],
	                index: index,
	                parent: parentContext,
	                children: [],
	                level: parentContext.level + 1
	            });
	            index++;
	            _.each(_that.metrics, function (metric) {
	                var metricParser = KiiQueryDataParser_1.KiiMetricParsers[metric.type || 'default']
	                    || KiiQueryDataParser_1.KiiMetricParsers['default'];
	                metricParser(metric, _context);
	            });
	            parentContext.children.push(_context);
	            _.each(_that.children, function (child) {
	                child.parseMetrics(_context);
	            });
	        });
	    };
	    AggregationNode.prototype.retrieval = function (func) {
	        var _that = this;
	        retrievalNode(this, func);
	        function retrievalNode(node, func, after) {
	            if (after === void 0) { after = false; }
	            if (!_that.fieldName && !after) {
	                func.apply(_that, [_that]);
	            }
	            node.children.forEach(function (child) {
	                retrievalNode(child, func);
	            });
	            if (!_that.fieldName && after) {
	                func.apply(_that, [_that]);
	            }
	        }
	    };
	    return AggregationNode;
	}());
	exports.AggregationNode = AggregationNode;


/***/ },

/***/ 138:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var KiiQueryUtils_1 = __webpack_require__(60);
	var AggregationNode_1 = __webpack_require__(86);
	var KiiQueryServiceProvider_1 = __webpack_require__(141);
	var KiiQueryDataParser_1 = __webpack_require__(87);
	var constants_1 = __webpack_require__(61);
	var KiiQueryConfig_1 = __webpack_require__(43);
	var Observable_1 = __webpack_require__(298);
	var KiiQuery = (function () {
	    function KiiQuery(queryStr) {
	        this._kiiQueryService = KiiQueryServiceProvider_1.KiiQueryServiceProvider.factoryKiiQueryService();
	        this._queryResult = function (_that) {
	            return new Observable_1.Observable(function (observer) {
	                _that._queryObserver = observer;
	            });
	        }(this);
	        if (queryStr)
	            this.setQueryStr(queryStr);
	    }
	    KiiQuery.prototype.getQueryPath = function () {
	        return this._queryPath;
	    };
	    KiiQuery.prototype.getAggTree = function () {
	        return this._aggTree;
	    };
	    KiiQuery.prototype.setTimeAgg = function (timeAgg) {
	        this._timeAgg = timeAgg;
	    };
	    KiiQuery.prototype.setTimePeriod = function (timePeriod) {
	        this._timePeriod = timePeriod;
	    };
	    KiiQuery.prototype.setQueryStr = function (queryStr) {
	        var queryJSON = null;
	        try {
	            queryJSON = KiiQueryUtils_1.KiiQueryUtils.parseQueryStr(queryStr);
	        }
	        catch (e) {
	            throw e;
	        }
	        this._originStr = queryStr;
	        this._originQueryJSON = queryJSON;
	        this._queryJSON = KiiQueryUtils_1.KiiQueryUtils.clone(this._originQueryJSON);
	        this._initializeQuery();
	    };
	    KiiQuery.prototype._initializeQuery = function () {
	        var queryJSON = this._queryJSON;
	        if (!queryJSON)
	            throw new Error('query is not provided!');
	        queryJSON.query = queryJSON.query || {};
	        queryJSON.query.filtered = queryJSON.query.filtered || {};
	        queryJSON.query.filtered.query = queryJSON.query.filtered.query || {};
	        queryJSON.query.filtered.filter = queryJSON.query.filtered.filter || {};
	        queryJSON.query.filtered.filter.bool = queryJSON.query.filtered.filter.bool || {};
	        queryJSON.query.filtered.filter.bool.must = queryJSON.query.filtered.filter.bool.must || [];
	        this._aggs = this._queryJSON.aggs;
	        this._query = this._queryJSON.query;
	        this._aggTree = this._buildAggTree();
	        this._summaryAggNode = this._aggTree;
	        this._aggTree = this._aggTree.children[0];
	        var dateFilter = { range: {} };
	        dateFilter.range[KiiQueryConfig_1.KiiQueryConfig.instance.DATE_FIELD] = {
	            gte: 0,
	            lte: 0,
	            format: 'epoch_millis'
	        };
	        this._timeFilter = KiiQueryUtils_1.KiiQueryUtils.findDateFilter(this._query.filtered.filter.bool.must);
	        if (!this._timeFilter) {
	            this._query.filtered.filter.bool.must.push(dateFilter);
	            this._timeFilter = dateFilter.range[KiiQueryConfig_1.KiiQueryConfig.instance.DATE_FIELD];
	        }
	        Object.assign(this._timeFilter, {
	            gte: 0,
	            lte: 0,
	            format: 'epoch_millis'
	        });
	        Object.assign(this, {
	            _queryName: KiiQueryUtils_1.KiiQueryUtils.getQueryName(this._queryJSON),
	            _queryPath: KiiQueryUtils_1.KiiQueryUtils.getQueryPath(this._queryJSON)
	        });
	    };
	    KiiQuery.prototype.getQueryName = function () {
	        return this._queryName;
	    };
	    KiiQuery.prototype.restore = function () {
	        this.setQueryStr(this._originStr);
	    };
	    KiiQuery.prototype.build = function () {
	        this._timeFilter.gte = this._timePeriod.fromTime;
	        this._timeFilter.lte = this._timePeriod.toTime;
	        if (this._timeAgg) {
	            this._timeAgg.field = KiiQueryConfig_1.KiiQueryConfig.instance.DATE_FIELD;
	            this._timeAgg.interval = this._timePeriod.interval + this._timePeriod.unit;
	        }
	        var _query = KiiQueryUtils_1.KiiQueryUtils.clone(this._queryJSON);
	        if (this._aggTree) {
	            this._aggTree.compile(KiiQueryUtils_1.KiiQueryUtils.getAggregations(_query));
	        }
	        KiiQueryUtils_1.KiiQueryUtils.removeKiiFields(_query);
	        return _query;
	    };
	    KiiQuery.prototype.query = function () {
	        var _this = this;
	        var _that = this;
	        var $defer = this._kiiQueryService.query(this.getQueryPath(), JSON.stringify(this.build()));
	        $defer.done(function (response) {
	            _this.parseData(response);
	        }).fail(function (error) {
	            Observable_1.Observable.throw(new Error('query failed.'));
	        });
	    };
	    KiiQuery.prototype.parseData = function (data) {
	        var _data = null;
	        var _summary = null;
	        var ___currentTime = (new Date()).getTime();
	        var ___runTime = (new Date()).getTime();
	        if (this._aggTree) {
	            this._aggTree.parse(data.aggregations);
	            ___runTime = (new Date()).getTime();
	            console.log("aggTree parse time: " + (___runTime - ___currentTime));
	            ___currentTime = (new Date()).getTime();
	            this._getFullKeySets(data.aggregations);
	            ___runTime = (new Date()).getTime();
	            console.log("get full key sets time: " + (___runTime - ___currentTime));
	            ___currentTime = (new Date()).getTime();
	            _data = this._dataParser(data.aggregations);
	            ___runTime = (new Date()).getTime();
	            console.log("parse data time: " + (___runTime - ___currentTime));
	            ___currentTime = (new Date()).getTime();
	            this._fillGap(_data);
	            ___runTime = (new Date()).getTime();
	            console.log("fillgap time: " + (___runTime - ___currentTime));
	            ___currentTime = (new Date()).getTime();
	            var _context = new KiiQueryDataParser_1.KiiMetricParserContext({
	                doc: _data,
	                index: null,
	                docs: [],
	                parent: {},
	                children: [],
	                level: 0
	            });
	            this._aggTree.parseMetrics(_context);
	            ___runTime = (new Date()).getTime();
	            console.log("parseMetrics time: " + (___runTime - ___currentTime));
	            ___currentTime = (new Date()).getTime();
	        }
	        _summary = this._parseSummary(data.aggregations);
	        this._queryObserver.next({ data: _data, summary: _summary });
	    };
	    KiiQuery.prototype.subscribe = function (func) {
	        this._queryResult.subscribe(func);
	    };
	    KiiQuery.prototype._parseSummary = function (data) {
	        var summary = {};
	        _.each(this._summaryAggNode.metrics, function (metric) {
	            summary[metric.fieldName] = data[metric.fieldName].value;
	        });
	        return summary;
	    };
	    KiiQuery.prototype._buildAggTree = function () {
	        var aggTree = extractAggFields(this._queryJSON, null, this, []);
	        var agg = KiiQueryUtils_1.KiiQueryUtils.getAggregations(this._queryJSON);
	        _.each(agg, function (value, fieldName) {
	            if (!KiiQueryUtils_1.KiiQueryUtils.isAgg(value)) {
	                var metric = {
	                    type: KiiQueryUtils_1.KiiQueryUtils._extractMetricType(agg[fieldName]),
	                    fieldName: fieldName,
	                    displayName: agg[fieldName][constants_1.constants.KiiFields.AGG_FIELD_DISPLAY_NAME],
	                    gapStrategy: 'zero',
	                    script: agg[fieldName].script,
	                    group: null,
	                    chart: null,
	                    selected: false
	                };
	                aggTree.metrics.push(metric);
	            }
	        });
	        return aggTree;
	        function extractAggFields(obj, name, _query, trace) {
	            var aggNode;
	            trace = trace || [];
	            if (KiiQueryUtils_1.KiiQueryUtils.isAgg(obj)) {
	                aggNode = new AggregationNode_1.AggregationNode;
	                _.extend(aggNode, {
	                    fieldName: name,
	                    displayName: obj[constants_1.constants.KiiFields.AGG_FIELD_DISPLAY_NAME],
	                    children: [],
	                    metrics: [],
	                    chart: obj[constants_1.constants.KiiFields.AGG_CHART],
	                    trace: trace
	                });
	                if (KiiQueryUtils_1.KiiQueryUtils._isTimeAgg(obj)) {
	                    if (!_query._timeAgg) {
	                        _query.setTimeAgg(obj[constants_1.constants.ESAggregationMethods.TIME]);
	                    }
	                    aggNode.displayName = constants_1.constants.DATE_DISPLAY_NAME;
	                    aggNode.type = 'time';
	                    aggNode.aggMethod = 'time';
	                }
	                else if (KiiQueryUtils_1.KiiQueryUtils._isEnumAgg(obj)) {
	                    aggNode.displayName = obj[constants_1.constants.KiiFields.AGG_FIELD_DISPLAY_NAME];
	                    aggNode.type = 'category';
	                    aggNode.keys = obj[constants_1.constants.ESAggregationMethods.ENUM].keys;
	                    aggNode.aggMethod = 'enum';
	                }
	                else if (KiiQueryUtils_1.KiiQueryUtils._isValueAgg(obj)) {
	                    aggNode.displayName = obj[constants_1.constants.KiiFields.AGG_FIELD_DISPLAY_NAME];
	                    aggNode.type = 'value';
	                    aggNode.aggMethod = 'default';
	                }
	                else if (KiiQueryUtils_1.KiiQueryUtils._isFilterAgg(obj)) {
	                    aggNode.displayName = obj[constants_1.constants.KiiFields.AGG_FIELD_DISPLAY_NAME];
	                    aggNode.type = 'category';
	                    aggNode.aggMethod = 'filter';
	                }
	                else {
	                    aggNode.displayName = obj[constants_1.constants.KiiFields.AGG_NAME];
	                    aggNode.type = 'category';
	                    aggNode.aggMethod = 'default';
	                }
	                var aggs = KiiQueryUtils_1.KiiQueryUtils.getAggregations(obj);
	                if (aggs) {
	                    for (var fieldName in aggs) {
	                        var _trace = aggNode.trace.concat([aggNode.children.length]);
	                        var child = extractAggFields(aggs[fieldName], fieldName, _query, _trace);
	                        if (child) {
	                            aggNode.children.push(child);
	                        }
	                        else if (KiiQueryUtils_1.KiiQueryUtils.isBucketAggMeasure(aggs[fieldName]) || KiiQueryUtils_1.KiiQueryUtils.isAggMeasure(aggs[fieldName])) {
	                            aggNode.metrics.push({
	                                selected: aggs[fieldName][constants_1.constants.KiiFields.METRIC_SELECT] || false,
	                                type: KiiQueryUtils_1.KiiQueryUtils._extractMetricType(aggs[fieldName]),
	                                displayName: aggs[fieldName][constants_1.constants.KiiFields.AGG_FIELD_DISPLAY_NAME],
	                                fieldName: fieldName,
	                                group: aggs[fieldName][constants_1.constants.KiiFields.AGG_GROUP],
	                                chart: aggs[fieldName][constants_1.constants.KiiFields.AGG_CHART],
	                                script: aggs[fieldName].script,
	                                gapStrategy: aggs[constants_1.constants.KiiFields.METRIC_GAP_STRAT] || 'zero'
	                            });
	                        }
	                    }
	                }
	            }
	            else {
	                var aggs = KiiQueryUtils_1.KiiQueryUtils.getAggregations(obj);
	                if (aggs) {
	                    aggNode = new AggregationNode_1.AggregationNode;
	                    _.extend(aggNode, {
	                        fieldName: name,
	                        chart: obj[constants_1.constants.KiiFields.AGG_CHART],
	                        displayName: obj[constants_1.constants.KiiFields.AGG_FIELD_DISPLAY_NAME],
	                        children: [],
	                        metrics: [],
	                        trace: []
	                    });
	                    for (var fieldName in aggs) {
	                        var child = extractAggFields(aggs[fieldName], fieldName, _query, [aggNode.trace.length]);
	                        if (child) {
	                            aggNode.children.push(child);
	                        }
	                    }
	                }
	            }
	            return aggNode;
	        }
	    };
	    KiiQuery.prototype._getFullKeySets = function (data) {
	        retrieval(this._aggTree, this._aggTree, data[this._aggTree.fieldName]);
	        function retrieval(rootNode, currentNode, data) {
	            currentNode.keySet = query(rootNode, currentNode, data);
	            _.each(currentNode.children, function (child) {
	                retrieval(rootNode, child, data);
	            });
	        }
	        function query(rootNode, targetNode, data) {
	            var trace = targetNode.trace.concat([]).splice(rootNode.trace.length);
	            if (trace.length) {
	                var keySets_1 = [];
	                _.each(data.buckets, function (bucket) {
	                    var _trace = trace.concat([]);
	                    var i = _trace.shift();
	                    var child = rootNode.children[i];
	                    var keys = query(child, targetNode, bucket[child.fieldName]);
	                    keySets_1 = _.union(keySets_1, keys);
	                });
	                return keySets_1;
	            }
	            else {
	                return _.pluck(data.buckets, 'key');
	            }
	        }
	    };
	    KiiQuery.prototype._dataParser = function (data) {
	        function retrieval(aggNode, data) {
	            var _data = {};
	            _data[aggNode.fieldName] = {};
	            _.each(data.buckets, function (bucket) {
	                var _child = {};
	                _child[bucket.key] = {};
	                _.each(aggNode.children, function (childNode) {
	                    _.extend(_child[bucket.key], retrieval(childNode, bucket[childNode.fieldName]));
	                });
	                _.each(aggNode.metrics, function (agg) {
	                    _child[bucket.key][agg.fieldName] = bucket[agg.fieldName].value;
	                });
	                _.extend(_data[aggNode.fieldName], _child);
	            });
	            return _data;
	        }
	        return retrieval(this._aggTree, data[this._aggTree.fieldName]);
	    };
	    KiiQuery.prototype._fillGap = function (data) {
	        var _that = this;
	        retrieval(this._aggTree, data[this._aggTree.fieldName]);
	        function retrieval(currentNode, data) {
	            _.each(currentNode.keySet, function (key) {
	                data[key] = data[key] || {};
	                var _data = data[key];
	                _.each(currentNode.children, function (childNode) {
	                    if (!_data[childNode.fieldName]) {
	                        _data[childNode.fieldName] = {};
	                    }
	                    retrieval(childNode, _data[childNode.fieldName]);
	                });
	                _.each(currentNode.metrics, function (metric) {
	                    _data[metric.fieldName] = _data[metric.fieldName] || 0;
	                });
	            });
	        }
	    };
	    return KiiQuery;
	}());
	exports.KiiQuery = KiiQuery;


/***/ },

/***/ 43:
/***/ function(module, exports) {

	"use strict";
	var KiiQueryConfig = (function () {
	    function KiiQueryConfig(config) {
	        this.DATE_FIELD = 'date';
	        if (config) {
	            this._site = config.site;
	            this._port = config.port;
	            this._token = config.token;
	            this._chartOptions = config.chartOptions;
	        }
	    }
	    KiiQueryConfig.prototype.getSite = function () {
	        return this._site;
	    };
	    KiiQueryConfig.prototype.getPort = function () {
	        return this._port;
	    };
	    KiiQueryConfig.prototype.getBaseUrl = function () {
	        return this._site + ':' + this._port;
	    };
	    KiiQueryConfig.prototype.getToken = function () {
	        return this._token;
	    };
	    KiiQueryConfig.prototype.getChartOptions = function () {
	        return this._chartOptions;
	    };
	    KiiQueryConfig.setConfig = function (config) {
	        this.instance._site = config.site || this.instance._site;
	        this.instance._port = config.port || this.instance._port;
	        this.instance._token = config.token || this.instance._token;
	        this.instance.DATE_FIELD = config.timeStampField || this.instance.DATE_FIELD;
	        this.instance._chartOptions = config.chartOptions || this.instance._chartOptions;
	    };
	    KiiQueryConfig.getConfig = function () {
	        return this.instance;
	    };
	    KiiQueryConfig.instance = new KiiQueryConfig();
	    return KiiQueryConfig;
	}());
	exports.KiiQueryConfig = KiiQueryConfig;


/***/ },

/***/ 139:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var KiiQueryUtils_1 = __webpack_require__(60);
	var constants_1 = __webpack_require__(61);
	var scriptMetricCompiler = function (parentJSON, metric) {
	    if (parentJSON[metric.fieldName][constants_1.constants.KiiFields.SERIES_NAME]) {
	        metric.seriesName = parentJSON[metric.fieldName][constants_1.constants.KiiFields.SERIES_NAME];
	    }
	    delete parentJSON[metric.fieldName];
	};
	var defaultMetricCompiler = function (parentJSON, metric) {
	    if (parentJSON[metric.fieldName][constants_1.constants.KiiFields.SERIES_NAME]) {
	        metric.seriesName = parentJSON[metric.fieldName][constants_1.constants.KiiFields.SERIES_NAME];
	    }
	    KiiQueryUtils_1.KiiQueryUtils.removeKiiFields(parentJSON[metric.fieldName]);
	};
	exports.KiiMetricCompilers = {
	    'script': scriptMetricCompiler,
	    'default': defaultMetricCompiler
	};
	var enumAggCompiler = function (queryJSON, kiiAgg) {
	    var _target = queryJSON[kiiAgg.fieldName];
	    var _parent = queryJSON;
	    var _keys = _target['enum'].keys;
	    var _values = _target['enum'].values;
	    var _field = _target['enum'].field;
	    delete _parent[kiiAgg.fieldName];
	    delete _target['enum'];
	    KiiQueryUtils_1.KiiQueryUtils.removeKiiFields(_target);
	    _.each(_keys, function (key, i) {
	        var _result = KiiQueryUtils_1.KiiQueryUtils.clone(_target);
	        var _terms = {};
	        _terms[_field] = _values[i];
	        _result.filter = {
	            'terms': _terms
	        };
	        _parent[key] = _result;
	    });
	};
	var defaultAggCompiler = function (queryJSON, kiiAgg) {
	    var _result = queryJSON[kiiAgg.fieldName];
	    KiiQueryUtils_1.KiiQueryUtils.removeKiiFields(_result);
	    delete queryJSON[kiiAgg.fieldName];
	    queryJSON[kiiAgg.fieldName] = _result;
	};
	var timeAggCompiler = function (queryJSON, kiiAgg) {
	    exports.KiiAggregationCompilers['default'](queryJSON, kiiAgg);
	};
	exports.KiiAggregationCompilers = {
	    'time': timeAggCompiler,
	    'enum': enumAggCompiler,
	    'default': defaultAggCompiler,
	};


/***/ },

/***/ 87:
/***/ function(module, exports) {

	"use strict";
	var KiiMetricParserContext = (function () {
	    function KiiMetricParserContext(_context) {
	        _.extend(this, _context);
	    }
	    KiiMetricParserContext.prototype.offset = function (level, offset) {
	        var dLevel = this.level - level;
	        var context = this;
	        var trace = [];
	        while (dLevel > 0) {
	            trace.push(context.index);
	            context = this.parent;
	            dLevel--;
	        }
	        context = context.parent.children[context.index + offset];
	        if (!context) {
	            console.log(Error('context out of bound.'));
	            return null;
	        }
	        while (trace.length) {
	            context = context.children[trace.pop()];
	        }
	        return context;
	    };
	    return KiiMetricParserContext;
	}());
	exports.KiiMetricParserContext = KiiMetricParserContext;
	exports.scriptMetricParser = function (metric, context) {
	    context.doc[metric.fieldName] = 0;
	    (function (context) {
	        var doc = context.doc;
	        var index = context.index;
	        var parent = context.parent;
	        var offset = context.offset;
	        try {
	            context.doc[metric.fieldName] = eval(metric.script);
	        }
	        catch (e) {
	            console.log(e);
	        }
	    })(context);
	    exports.defaultMetricParser(metric, context);
	};
	exports.defaultMetricParser = function (metric, context) {
	    context.doc[metric.fieldName] = parseFloat(Number.prototype.toFixed.call(context.doc[metric.fieldName], 3));
	};
	exports.KiiMetricParsers = {
	    'script': exports.scriptMetricParser,
	    'default': exports.defaultMetricParser
	};
	var filterAggParser = function (resJSON, kiiAgg) {
	    var _bucket = resJSON[kiiAgg.fieldName];
	    _bucket['key'] = kiiAgg.fieldName;
	    var _buckets = [_bucket];
	    resJSON[kiiAgg.fieldName] = {
	        buckets: _buckets
	    };
	};
	var enumAggParser = function (resJSON, kiiAgg) {
	    var _buckets = [];
	    _.each(kiiAgg.keys, function (key) {
	        var _bucket = resJSON[key];
	        _bucket['key'] = key;
	        _buckets.push(_bucket);
	        delete resJSON[key];
	    });
	    resJSON[kiiAgg.fieldName] = {
	        buckets: _buckets
	    };
	};
	var defaultAggParser = function (resJSON, kiiAgg) {
	};
	exports.KiiAggDataParsers = {
	    'filter': filterAggParser,
	    'enum': enumAggParser,
	    'default': defaultAggParser
	};


/***/ },

/***/ 140:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var KiiQueryConfig_1 = __webpack_require__(43);
	var KiiQueryService = (function () {
	    function KiiQueryService() {
	    }
	    KiiQueryService.prototype.query = function (path, queryString) {
	        var headers = {};
	        var config = KiiQueryConfig_1.KiiQueryConfig.getConfig();
	        if (config.getToken()) {
	            headers['Authorization'] = config.getToken();
	        }
	        var settings = {
	            method: 'POST',
	            url: config.getBaseUrl() + path,
	            data: queryString,
	            headers: headers,
	            contentType: "application/json",
	            dataType: 'json'
	        };
	        return $.ajax(settings);
	    };
	    return KiiQueryService;
	}());
	exports.KiiQueryService = KiiQueryService;


/***/ },

/***/ 141:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var KiiQueryService_1 = __webpack_require__(140);
	var KiiQueryServiceProvider = (function () {
	    function KiiQueryServiceProvider() {
	    }
	    KiiQueryServiceProvider.prototype._factoryKiiQueryService = function () {
	        this._kiiQueryService = new KiiQueryService_1.KiiQueryService();
	        return this._kiiQueryService;
	    };
	    KiiQueryServiceProvider.factoryKiiQueryService = function () {
	        return this.kiiQueryServiceProvider._kiiQueryService ?
	            this.kiiQueryServiceProvider._kiiQueryService :
	            this.kiiQueryServiceProvider._factoryKiiQueryService();
	    };
	    KiiQueryServiceProvider.kiiQueryServiceProvider = new KiiQueryServiceProvider;
	    return KiiQueryServiceProvider;
	}());
	exports.KiiQueryServiceProvider = KiiQueryServiceProvider;


/***/ },

/***/ 60:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var constants_1 = __webpack_require__(61);
	var KiiQueryConfig_1 = __webpack_require__(43);
	exports.KiiQueryUtils = {
	    removeKiiFields: function (obj) {
	        if (!obj || typeof obj != 'object')
	            return;
	        for (var kiiFieldName in constants_1.constants.KiiFields) {
	            if (exports.KiiQueryUtils._isNotNull(obj[constants_1.constants.KiiFields[kiiFieldName]])) {
	                delete obj[constants_1.constants.KiiFields[kiiFieldName]];
	            }
	        }
	    },
	    getQueryName: function (obj) {
	        return obj[constants_1.constants.KiiFields.AGG_NAME];
	    },
	    getQueryPath: function (obj) {
	        return obj[constants_1.constants.KiiFields.QUERY_PATH];
	    },
	    parseQueryStr: function (queryStr) {
	        var queryJSON;
	        try {
	            queryJSON = JSON.parse(queryStr);
	        }
	        catch (e) {
	            throw new Error('failed convert query string to JSON');
	        }
	        return queryJSON;
	    },
	    findDateFilter: function (must) {
	        for (var _i = 0, must_1 = must; _i < must_1.length; _i++) {
	            var condition = must_1[_i];
	            if (condition.range && condition.range[KiiQueryConfig_1.KiiQueryConfig.instance.DATE_FIELD]) {
	                return condition.range[KiiQueryConfig_1.KiiQueryConfig.instance.DATE_FIELD];
	            }
	        }
	    },
	    getAggregations: function (obj) {
	        return obj.aggs || obj.aggregations;
	    },
	    _isEnumAgg: function (obj) {
	        if (!obj)
	            return false;
	        var _enumMethod = constants_1.constants.ESAggregationMethods.ENUM;
	        return !!obj[_enumMethod];
	    },
	    _isTimeAgg: function (obj) {
	        if (!obj)
	            return false;
	        var _timeMethod = constants_1.constants.ESAggregationMethods.TIME;
	        return !!obj[_timeMethod];
	    },
	    _isValueAgg: function (obj) {
	        if (!obj)
	            return false;
	        var _valueMethods = constants_1.constants.ESAggregationMethods.VALUE;
	        for (var i in _valueMethods) {
	            if (!!obj[_valueMethods[i]]) {
	                return true;
	            }
	        }
	        if (obj[constants_1.constants.KiiFields.AGG_CHART] === 'line')
	            return true;
	        return false;
	    },
	    _isFilterAgg: function (obj) {
	        if (!obj)
	            return false;
	        var _timeMethod = constants_1.constants.ESAggregationMethods.FILTER;
	        return !!obj[_timeMethod];
	    },
	    _isCategoryAgg: function (obj) {
	        if (!obj)
	            return false;
	        var _categoryMethods = constants_1.constants.ESAggregationMethods.CATEGORY;
	        for (var i in _categoryMethods) {
	            if (!!obj[_categoryMethods[i]]) {
	                return true;
	            }
	        }
	        return false;
	    },
	    isAgg: function (obj) {
	        var flag = false;
	        for (var _i = 0, _a = constants_1.constants.AggMethods; _i < _a.length; _i++) {
	            var k = _a[_i];
	            flag = flag || !!obj[k];
	        }
	        return flag;
	    },
	    isAggMeasure: function (obj) {
	        var flag = false;
	        for (var _i = 0, _a = constants_1.constants.AggMeasures; _i < _a.length; _i++) {
	            var k = _a[_i];
	            flag = flag || obj[k];
	        }
	        return flag;
	    },
	    isBucketAggMeasure: function (obj) {
	        var flag = false;
	        for (var _i = 0, _a = constants_1.constants.BucketAggMeasures; _i < _a.length; _i++) {
	            var k = _a[_i];
	            flag = flag || obj[k];
	        }
	        return flag;
	    },
	    _extractMetricType: function (obj) {
	        for (var _i = 0, _a = constants_1.constants.AggMeasures; _i < _a.length; _i++) {
	            var k = _a[_i];
	            if (obj[k])
	                return k;
	        }
	        for (var _b = 0, _c = constants_1.constants.BucketAggMeasures; _b < _c.length; _b++) {
	            var k = _c[_b];
	            if (obj[k])
	                return k;
	        }
	        return null;
	    },
	    _isNotNull: function (val) {
	        if (val || val === 0 || val === false || val === null) {
	            return true;
	        }
	        return false;
	    },
	    clone: function (obj) {
	        return JSON.parse(JSON.stringify(obj));
	    }
	};


/***/ },

/***/ 61:
/***/ function(module, exports) {

	"use strict";
	exports.constants = {
	    DATE_DISPLAY_NAME: '时间',
	    KiiFields: {
	        AGG_FIELD_DISPLAY_NAME: '_kii_agg_field_name',
	        QUERY_PATH: '_kii_query_path',
	        AGG_NAME: '_kii_agg_name',
	        AGG_GROUP: '_kii_agg_group',
	        AGG_CHART: '_kii_agg_chart',
	        METRIC_GAP_STRAT: '_kii_gap_strat',
	        METRIC_SELECT: '_kii_selected',
	        SERIES_NAME: '_kii_series_name'
	    },
	    ESAggregationMethods: {
	        TIME: 'date_histogram',
	        ENUM: 'enum',
	        CATEGORY: ['terms', 'term'],
	        VALUE: ['range'],
	        FILTER: 'filter'
	    },
	    AggMethods: [
	        'histogram', 'date_histogram',
	        'date_range', 'range',
	        'terms', 'enum', 'filter'
	    ],
	    AggMeasures: [
	        'avg', 'sum', 'min', 'max', 'value_count',
	        'script'
	    ],
	    BucketAggMeasures: [
	        'avg_bucket', 'sum_bucket', 'count_bucket', 'min_bucket', 'max_bucket'
	    ]
	};


/***/ },

/***/ 88:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	function __export(m) {
	    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
	}
	__export(__webpack_require__(138));
	__export(__webpack_require__(86));
	__export(__webpack_require__(43));


/***/ },

/***/ 142:
/***/ function(module, exports) {

	"use strict";
	var KiiTimePeriod = (function () {
	    function KiiTimePeriod(argument) {
	    }
	    KiiTimePeriod.prototype.setUnit = function (unit) {
	        this.unit = unit;
	    };
	    KiiTimePeriod.prototype.setInterval = function (interval) {
	        this.interval = interval;
	    };
	    KiiTimePeriod.prototype.setFromTime = function (fromTime) {
	        if (!fromTime) {
	            throw new Error('from time cannot be null.');
	        }
	        if (!(fromTime instanceof Date)) {
	            throw new Error('from time is not instance of date.');
	        }
	        this.fromTime = fromTime.getTime();
	    };
	    KiiTimePeriod.prototype.setToTime = function (toTime) {
	        if (!toTime) {
	            this.toTime = null;
	            return;
	        }
	        this.toTime = toTime.getTime();
	    };
	    return KiiTimePeriod;
	}());
	exports.KiiTimePeriod = KiiTimePeriod;


/***/ },

/***/ 143:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	function __export(m) {
	    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
	}
	__export(__webpack_require__(142));


/***/ },

/***/ 144:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	function __export(m) {
	    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
	}
	__export(__webpack_require__(137));
	__export(__webpack_require__(88));
	__export(__webpack_require__(143));


/***/ },

/***/ 0:
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var KiiReporting_1 = __webpack_require__(144);
	(function (window) {
	    var KiiReporting = {
	        KiiChart: KiiReporting_1.KiiChart,
	        KiiQuery: KiiReporting_1.KiiQuery,
	        KiiTimePeriod: KiiReporting_1.KiiTimePeriod,
	        KiiQueryConfig: KiiReporting_1.KiiQueryConfig
	    };
	    window.KiiReporting = KiiReporting;
	})(window);


/***/ }

});
//# sourceMappingURL=KiiReporting.bundle.map