var localize = require('localize');

var kiiLan = new localize({
  'login': {
    'cn': '登录',
    'eg': 'Login'
  },
  'logout': {
    'cn': '注销',
    'eg': 'Logout'
  },
  'home_page': {
    'cn': '主页',
    'eg': 'Home'
  },
  'help': {
    'cn': '帮助',
    'eg': 'Help'
  },
  'edit_account': {
    'cn': '设置账户',
    'eg': 'Edit Account'
  },
  'enter_tag': {
    'cn': '进入',
    'eg': 'Enter'
  },
  'home_search_placeholder': {
    'cn': '输入标签、关键字...',
    'eg': 'type in tag or keywords...'
  },
  'tag_name': {
    'cn': '标签',
    'eg': 'Tag'
  },
  'sub_tag_name': {
    'cn': '名称',
    'eg': 'name'
  },
  // ### Sub tag related
  'provider': {
    'cn': '厂商',
    'eg': 'Provider'
  },
  'created_at': {
    'cn': '创建时间',
    'eg': 'Created At'
  },
  'updated_at': {
    'cn': '更新时间',
    'eg': 'Updated At'
  },
  'search': {
    'cn': '搜索',
    'eg': 'Search'
  },
  'information': {
    'cn': '基本信息',
    'eg': 'Information'
  },
  'request_arguements': {
    'cn': '请求参数',
    'eg': 'Arguements'
  },
  'example_code': {
    'cn': '示例代码',
    'eg': 'Example Code'
  },
  'error_code': {
    'cn': '异常码',
    'eg': 'Error Code'
  },
  'api_name': {
    'cn': '接口名',
    'eg': 'API Name'
  },
  'request_method': {
    'cn': '请求方法',
    'eg': 'Request Method'
  },
  'request_url': {
    'cn': '请求Url',
    'eg': 'Request Url'
  },
  'description': {
    'cn': '描述',
    'eg': 'Description'
  },
  'argement_name': {
    'cn': '参数名',
    'eg': 'Arguement Name'
  },
  'type': {
    'cn': '类型',
    'eg': 'Type'
  },
  'must': {
    'cn': '必填',
    'eg': 'Must'
  },
  'default_value': {
    'cn': '默认值',
    'eg': 'Default_value'
  },
  'arg_position': {
    'cn': '参数位置',
    'eg': 'Arguement Position'
  },
  'send_request': {
    'cn': '发送请求',
    'eg': 'Request'
  },
  'response': {
    'cn': '返回结果',
    'eg': 'Response'
  },
  'error_response': {
    'cn': '错误返回',
    'eg': 'Error Response'
  },
  'publish_service': {
    'cn': '发布服务',
    'eg': 'Publish Service'
  },
  'questions': {
    'cn': '常见问题',
    'eg': 'Questions'
  },
  'air_conditioner': {
    'cn': '空调',
    'eg': 'Air Conditioner'
  },
  'rule_engine': {
    'cn': '规则引擎',
    'eg': 'Rule Engine'
  },
  'geo_location': {
    'cn': '地理位置',
    'eg': 'Geo Location'
  },
  'property': {
    'cn': '属性',
    'eg': 'Property'
  },
  'action': {
    'cn': '命令',
    'eg': 'Action'
  },
  'application_layer': {
    'cn': '应用层',
    'eg': 'Application '
  },
  'device_layer': {
    'cn': '设备层',
    'eg': 'Device '
  },
  'category': {
    'cn': '所属分类',
    'eg': 'Category'
  },
  'update_time': {
    'cn': '更新时间',
    'eg': 'Update Time'
  },
  'service_description': {
    'cn': '服务描述',
    'eg': 'Service Description'
  },
  'basic_information': {
    'cn': '基本信息',
    'eg': 'Basic Information'
  },
  'request': {
    'cn': '请求',
    'eg': 'Request'
  },
  'request_parameter': {
    'cn': '请求参数',
    'eg': 'Request Parameter'
  },
  'parameter_name': {
    'cn': '参数名',
    'eg': 'Parameter Name'
  },
  'required': {
    'cn': '必填',
    'eg': 'Required'
  },
  'request_expired': {
    'cn': '请求过期',
    'eg': 'Request Expired'
  },
  'message_protocal': {
    'cn': '消息协议',
    'eg': 'Message Protocal'
  },
  'subscribe_address': {
    'cn': '订阅地址',
    'eg': 'Subscribe Address'
  },
  'cancel_subscribe': {
    'cn': '取消订阅',
    'eg': 'Cancel Subscribe'
  },
  'filter': {
    'cn': '筛选搜索',
    'eg': 'Filter'
  },
  'choose_entrance': {
    'cn': '选择进入',
    'eg': 'Choose Entrance'
  },
  'download_files': {
    'cn': '资料下载',
    'eg': 'Files'
  },
  'current_version': {
    'cn': '当前版本',
    'eg': 'Current Version'
  }
});

var kiiLocalize = function (req) {

  var lan = req.params['l'];

  switch (lan) {
    case 'cn':
    case 'eg':
      break;
    default:
      lan = 'eg';
      break;
  }

  kiiLan.setLocale(lan);

  var pageContents = {
    home_page: kiiLan.translate('home_page'),
    help: kiiLan.translate('help'),
    login: kiiLan.translate('login'),
    logout: kiiLan.translate('logout'),
    edit_account: kiiLan.translate('edit_account'),
    publish_service: kiiLan.translate('publish_service'),
    questions: kiiLan.translate('questions'),
    air_conditioner: kiiLan.translate('air_conditioner'),
    rule_engine: kiiLan.translate('rule_engine'),
    property: kiiLan.translate('property'),
    action: kiiLan.translate('action'),
    geo_location: kiiLan.translate('geo_location'),
    download_files: kiiLan.translate('download_files'),
    lan: lan
  };

  return pageContents;
};

module.exports.kiiLocalize = kiiLocalize;
module.exports.kiiLan = kiiLan;