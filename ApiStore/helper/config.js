var express = require('express');
var app = express();

var settings = {
  LOCAL: {
    proxyServerUrl: 'http://localhost:9091/3rdpartyapiserver/api',
    ip: 'http://114.215.196.178',
    kiiCloud: {
      appID: 'a0370808',
      appKey: '4e126a56657a1c410a0717decc6bc69c',
      clientID: '5d6b309afe30f7235a3a584916eabcac',
      clientSecret: 'c64827a14e91848c312233afc61c7ac1c74956ffb38ba48ed6ce977af6727275',
      endpoint: 'https://api-development-beehivecn3.internal.kii.com/api'
    }
  },
  DEMO: {
    proxyServerUrl: 'http://54.223.39.102:8081/3rdpartyapiserver/api',
    kiiCloud: {
      appID: '39eeccb8',
      appKey: 'f94a471dc4a25ffeb392a87bb1dd135c',
      clientID: '5e2691af828ac730faae125541757dd7',
      clientSecret: '1bb26037cf72b066fd391ca8db49b6f18c6739c5ff4e9a3b1d3e8379579ea76a',
      endpoint: 'https://api-jp.kii.com/api'
    }
  },
  DEV: {
    proxyServerUrl: 'http://114.215.196.178:8081/3rdpartyapiserver/api',
    ip: 'http://114.215.196.178',
    kiiCloud: {
      appID: 'a0370808',
      appKey: '4e126a56657a1c410a0717decc6bc69c',
      clientID: '5d6b309afe30f7235a3a584916eabcac',
      clientSecret: 'c64827a14e91848c312233afc61c7ac1c74956ffb38ba48ed6ce977af6727275',
      endpoint: 'https://api-development-beehivecn3.internal.kii.com/api'
    }
  },
  QA: {
    proxyServerUrl: 'http://api.openibplatform.com',
    ip: 'http://114.215.178.24',
    kiiCloud: {
      appID: 'a07e56dc',
      appKey: 'f600091915b2856a33351faf3df306ce',
      clientID: 'e1b30fff8fc4d27480f1ef3d3d012e62',
      clientSecret: '04cc9dc7ce4e38f32b1045b86d546a264f12669bcadf80de964cef8b87abd692',
      endpoint: 'https://api-development-beehivecn3.internal.kii.com/api'
    }
  },
  PRODUCTION: {
    proxyServerUrl: 'http://120.77.83.143:8081/3rdpartyapiserver/api/',
    ip: 'http://120.77.83.143',
    kiiCloud: {
      appID: 'wfenb44dvlyi',
      appKey: '6f5b2d230cae45a4ae36f856dedf8554',
      clientID: 'e1b30fff8fc4d27480f1ef3d3d012e62',
      clientSecret: '04cc9dc7ce4e38f32b1045b86d546a264f12669bcadf80de964cef8b87abd692',
      endpoint: 'https://api-beehivecn4.kii.com/api'
    }
  }
};

switch (app.get('env').toLocaleLowerCase()) {
  case 'development':
    module.exports = settings['DEV'];
    break;
  case 'production':
    module.exports = settings['PRODUCTION'];
    break;
  case 'qa':
    module.exports = settings['QA'];
    break;
  default:
    module.exports = settings[app.get('env').toLocaleUpperCase()];
    break;
}