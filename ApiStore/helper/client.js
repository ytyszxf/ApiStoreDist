'use strict';
var Client = require('node-rest-client').Client;
var client = new Client();

var methods = ['get', 'post', 'put', 'delete', 'patch'];

/**
 * convert query to promise style
 * @param  {[type]} method){               client[method] [description]
 * @return {[type]}           [description]
 */
for(let method of methods){
  client[method] = function(appliedMethod){
    return function(){
      var args = Array.prototype.slice.call(arguments);
      var defer = new Promise(function(resolve, reject){
        args.push(function(data, response){
          resolve(data);
        });
        appliedMethod.apply(client, args).on('error', function(){
          reject.apply(defer, arguments);
        });
      });
      return defer;
    }
  }(client[method]);
}


module.exports = client;
