var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var bodyParser = require('body-parser');
var routes = require('./routes/index');

var app = express();

var kiiLocalization = require('./helper/localize');
var kiiLocalize = kiiLocalization.kiiLocalize;
var kiiLan = kiiLocalization.kiiLan;

// view engine setup
app.set('view engine', 'jade');
app.set('view options', {debug: true});

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(session({
    secret: 'thirdPartyStore',
    resave: true,
    saveUninitialized: true
}));
app.use(express.static(path.join(__dirname, 'public')));

// config routers
routes(app);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  // var err = new Error('Not Found');
  // err.status = 404;
  // next(err);
});


// development error handler
// will print stacktrace
if (app.get('env').toLocaleLowerCase() === 'development') {
  console.log('In development!');
  app.set('views', path.join(__dirname, '/../client/.tmp'));
  //app.use(express.static(path.join(__dirname, '/../client/src')));
  //app.use(express.static(path.join(__dirname, '/../client')));

  /*
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);

    var pageContents = kiiLocalize(req);;
    pageContents.error = err;

    res.render('error/error', pageContents);
  });
  */
}
// production error handler
// no stacktraces leaked to user

if (app.get('env').toLocaleLowerCase() !== 'development') {
  app.use(express.static(path.join(__dirname, 'public')));
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);

    var pageContents = kiiLocalize(req);;
    pageContents.error = err;

    res.render('error/error', pageContents);
  });
}

console.log(app.get('env'));

module.exports = app;
