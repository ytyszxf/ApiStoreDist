'use strict';

var express = require('express');
var router = express.Router();
var _ = require('lodash');
var TSTag = require('../../models/TSTag');

var kiiLocalization = require('../../helper/localize');
var kiiLocalize = kiiLocalization.kiiLocalize;
var kiiLan = kiiLocalization.kiiLan;


router.get('/:tagId', function(req, res) {
  let pageContents = req.pageContents;
  let tagId = req.params.tagId;
  pageContents.enter_tag = kiiLan.translate('enter_tag');
  pageContents.provider = kiiLan.translate('provider');
  pageContents.created_at = kiiLan.translate('created_at');
  pageContents.updated_at = kiiLan.translate('updated_at');
  pageContents.sub_tag_name = kiiLan.translate('sub_tag_name');

  TSTag.findTagById(tagId).then(tag => {
    pageContents.naviLinks.push({name: tag.name, link: ''});
      TSTag.findSubTags(tagId).then(tags => {
        pageContents.tagId = tagId;
        pageContents.tags = tags;
        res.render('tag/tag', pageContents);
      });
  });
});

router.get('/:tagId/*', function(req, res, next) {
  let tagId = req.params.tagId;
  let pageContents = req.pageContents;
  let naviLinks = req.pageContents.naviLinks;

  TSTag.findTagById(tagId).then(tag => {
    naviLinks.push({name: tag.name, link: '/' + pageContents.lan + '/tags/' + tagId});
    next();
  });
});

module.exports = router;