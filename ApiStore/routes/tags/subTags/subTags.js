'use strict';

var express = require('express');
var router = express.Router();
var _ = require('lodash');
var TSApi = require('../../../models/TSApi');

var kiiLocalization = require('../../../helper/localize');
var kiiLocalize = kiiLocalization.kiiLocalize;
var kiiLan = kiiLocalization.kiiLan;

router.get('/:tagId', function(req, res, next) {
  var pageContents = req.pageContents;
  let tagId = req.params.tagId;

  pageContents.information = kiiLan.translate('information');
  pageContents.request_arguements = kiiLan.translate('request_arguements');
  pageContents.example_code = kiiLan.translate('example_code');
  pageContents.error_code = kiiLan.translate('error_code');

  pageContents.api_name = kiiLan.translate('api_name');
  pageContents.request_method = kiiLan.translate('request_method');
  pageContents.request_url = kiiLan.translate('request_url');
  pageContents.description = kiiLan.translate('description');
  pageContents.provider = kiiLan.translate('provider');
  pageContents.argement_name = kiiLan.translate('argement_name');

  pageContents.type = kiiLan.translate('type');
  pageContents.must = kiiLan.translate('must');
  pageContents.default_value = kiiLan.translate('default');
  pageContents.arg_position = kiiLan.translate('arg_position');
  pageContents.request = kiiLan.translate('request');
  pageContents.response = kiiLan.translate('response');
  pageContents.error_resposne = kiiLan.translate('error_resposne');

  TSApi.getByTagId(tagId).then(tag => {
    pageContents.tagId = tagId;
    pageContents.tag = tag;
    pageContents.naviLinks.push({name: tag.name, link: ''});
    res.render('tag/sub-tag/sub-tag', pageContents);
  });
});

module.exports = router;