'use strict';

var express = require('express');
var router = express.Router();
var _ = require('lodash');
var TSTag = require('../../models/TSTag');

var kiiLocalization = require('../../helper/localize');
var kiiLocalize = kiiLocalization.kiiLocalize;
var kiiLan = kiiLocalization.kiiLan;


router.get('/', function(req, res) {
  let pageContents = req.pageContents;
  
  pageContents.enter_tag = kiiLan.translate('enter_tag');
  pageContents.tag_name = kiiLan.translate('tag_name');
  pageContents.provider = kiiLan.translate('provider');
  pageContents.created_at = kiiLan.translate('created_at');
  pageContents.updated_at = kiiLan.translate('updated_at');
  pageContents.sub_tag_name = kiiLan.translate('sub_tag_name');
  pageContents.type = kiiLan.translate('type');

  pageContents.naviLinks.push({name:kiiLan.translate('search'), link:''});
  
  TSTag.query(req.query.query).then(tags=>{
    pageContents.resultSet = tags;

    res.render('search/search', pageContents);
  });
});

module.exports = router;