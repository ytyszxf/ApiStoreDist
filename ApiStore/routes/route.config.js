var tags = require('./tags/tags');
var subTags = require('./tags/subTags/subTags');
var search = require('./search/search');

var routers = [
  {path: '/:l/tags', router: tags},
  {path: '/:l/tags/:tagId/subtags', router: subTags},
  {path: '/:l/search', router: search}
];

module.exports.routers = routers;