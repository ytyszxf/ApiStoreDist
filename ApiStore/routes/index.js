'use strict';

var express = require('express');
var router = express.Router();
var _ = require('lodash');
var kiiLocalization = require('../helper/localize');
var kiiLocalize = kiiLocalization.kiiLocalize;
var kiiLan = kiiLocalization.kiiLan;
var TSTag = require('../models/TSTag');
var TSApi = require('../models/TSApi');
var TSUser = require('../models/TSUser');
var TSVisualMachine = require('../models/TSVisualMachine');
var TSApiExampleCode = require('../models/TSApiExampleCode');
var routeConfig = require('./route.config');

/* GET home page. */
/**
 * middlewire for language localization
 * @param  {[type]} req   [description]
 * @param  {[type]} res   [description]
 * @param  {[type]} next) {             var lan [description]
 * @return {[type]}       [description]
 */

router.all('*', function (req, res, next) {
  if (req.url == '/') {
    req.url = '/cn/guide';
  }
  next();
})

//first
router.all('/:l/*', getUser, function (req, res, next) {
  let filter = /^(.*\.(?!(htm|html|css|json|png|jpg|jpeg|ico|js|map)$))?[^.]*$/i;
  let flag = filter.test(req.originalUrl);
  if (!flag) {
    // next();
    return;
  }

  let pageContents = kiiLocalize(req);
  req.pageContents = pageContents || {};
  req.pageContents.naviLinks = [];
  let naviLinks = req.pageContents.naviLinks;
  naviLinks.push({
    name: kiiLan.translate('home_page'),
    link: '/' + pageContents.lan + '/index',
  });
  _.extend(req.pageContents, {
    otherLan: req.pageContents.lan == 'cn' ? 'eg' : 'cn',
  })
  console.log('session user', req.session.user);
  if (req.session.user) {
    _.extend(pageContents, {
      user: req.session.user
    })
  }
  next();
  //res.render('home/home', { title: 'Express' });
});

function getUser(req, res, next) {
  var Cookies = {};
  req.headers.cookie && req.headers.cookie.split(';').forEach(function (Cookie) {
    var parts = Cookie.split('=');
    Cookies[parts[0].trim()] = (parts[1] || '').trim();
  });
  req.session.Authorization = Cookies.Authorization;
  req.session.beehive_token = 'init';
  TSUser.getUser(Cookies.Authorization).then(user => {
    if (user.errorCode) {
      req.session.user = null
    } else {
      req.session.user = user;
    }
    TSUser.getApiKey(Cookies.Authorization).then(apiKey => {
      req.session.api_key = apiKey;
      TSUser.getBeehiveToken().then(token => {
        req.session.beehive_token = token;
        next();
      })
    })
  })
}

router.get('/:l/guide', function (req, res, next) {
  var pageContents = req.pageContents;
  _.extend(pageContents, {
    chooseEntrance: kiiLan.translate('choose_entrance'),
  });
  res.render('guide/guide', pageContents);
})

router.get('/:l/tutorial', function (req, res, next) {
  var pageContents = req.pageContents;
  res.render('tutorial/tutorial', pageContents);
})

/* GET home page. */
router.get('/:l/index', function (req, res, next) {
  TSTag.findMainTags().then(tags => {
    var pageContents = req.pageContents;
    _.extend(pageContents, {
      apis: tags,
      home_search_placeholder: kiiLan.translate('home_search_placeholder')
    });
    res.render('home/home', pageContents);
  }).catch(function (e) {
    console.log(e);
    res.render('home/home', pageContents);
  });
});

router.get('/:l/search/:name', function (req, res, next) {
  TSApi.findBySearch(req.params.name).then(apis => {
    var pageContents = req.pageContents;
    _.extend(pageContents, {
      apis: apis,
    });
    res.render('search/search', pageContents);
  }).catch(function (e) {
    console.log(e);
    res.render('search/search', pageContents);
  });
})

router.get('/:l/apiList/mainTag/:mainTag/subTag/:subTag', function (req, res, next) {
  let $defers = [];
  var mainTag = req.params.mainTag;
  var subTag = req.params.subTag;
  try {
    let $defer1 = TSApi.findVersionsByTagIDs(req.query.tagIDs);
    let $defer2 = TSTag.findMainTags();
    let $defer3 = TSTag.getByID(subTag);
    $defers = [$defer1, $defer2, $defer3];
  } catch (e) {
    console.log(e.stack);
  };

  Promise.all($defers).then((results) => {
    let apis = results[0];
    let tags = results[1];
    let subTagInfo = results[2];
    var pageContents = req.pageContents;
    _.extend(pageContents, {
      tags: tags,
      apis: apis,
      mainTag: mainTag,
      subTag: subTag,
      subTagInfo: subTagInfo,
      tagIDs: req.query.tagIDs ? req.query.tagIDs.split(',') : [],
      tagsLength: req.query.tagIDs ? req.query.tagIDs.split(',').length : 0,
      tagName: {
        'cn': 'tagName',
        'eg': 'englishName'
      },
      functionNameInLan: {
        'cn': 'functionDisplayName',
        'eg': 'functionName'
      },
      application_layer: kiiLan.translate('application_layer'),
      device_layer: kiiLan.translate('device_layer'),
      request_url: kiiLan.translate('request_url'),
      filter: kiiLan.translate('filter'),
    })

    res.render('apiList/apiList', pageContents);
  }).catch((error) => {
    console.log('error', e);
    res.render('apiList/apiList', pageContents);
  });
})

router.get('/:l/mainTag/:mainTag/subTag/:subTag/apiContent', function (req, res, next) {
  let mainTag = req.params.mainTag;
  let subTag = req.params.subTag;
  let $defers = [];
  try {
    let $defer1 = TSApi.getApiVersionByVersionID(req.query.apiID, req.query.versionID);
    let $defer2 = TSTag.getByID(mainTag);
    let $defer3 = TSTag.getByID(subTag);
    let $defer4 = TSApi.getTagsByApiID(req.query.apiID);
    let $defer5 = TSApiExampleCode.getByVersionID(req.query.versionID);
    let $defer6 = TSApiExampleCode.getResponseByVersionID(req.query.versionID);
    let $defer7 = TSTag.findMainTags();
    let $defer8 = TSVisualMachine.getDevices(req.session.beehive_token);

    $defers = [$defer1, $defer2, $defer3, $defer4, $defer5, $defer6, $defer7, $defer8];
  } catch (errorCode) {
    console.log("error", errorCode);
  }

  Promise.all($defers).then((results) => {
    console.log('enter promise');
    var pageContents = req.pageContents;
    var userLoggedIn = req.session.Authorization ? true : false;
    var beehiveUser = req.session.beehive_token ? true : false;
    var devices = [];
    var devicesFunctional = true;
    var correspondingDevices = false;
    //if user not loggin or can't get virtual devices (this may because of two reasons: beehive account bug or virtual device bug)
    if (!req.session.Authorization || results[7].code != 1) {
      devicesFunctional = false;
      console.log('devices functional false');
    }

    //if virtual devices are queryed, get corresponding devices with the same type as api device type
    if (devicesFunctional) {
      results[7].devices.forEach(function (device) {
        if (device.type == results[0].device) {
          devices.push(device);
        }
      })
      if (devices.length != 0) {
        correspondingDevices = true;
      }
      console.log('corresponding Devices', correspondingDevices);
    }
    var api = results[0];

    var curl = null;
    //if api is device type corresponding devices are queryed, curl can be generated
    if (api.schemaType && correspondingDevices) {

      curl = 'curl -X POST \\';
      var apiKeyHeader = `
      -H "apiKey:` + req.session.api_key[0].apiKey + '" \\';
      var authorizationHeader = `
      -H "Authorization: Bearer ` + req.session.beehive_token + '" \\';
      var contentTypeHeader = `
      -H "Content-Type: application/json" \\`;
      var body = `
      -d "{`;
      var thingList = '"thingList": ["' + devices[0].globalThingID + '"],';
      var otherPara = '';
      api.bodyList.forEach(function (para) {
        if (para.para_name != 'thingList') {
          otherPara = otherPara + '"' + para.para_name + '": "' + para.para_value + '"';
        }
      })
      var body = body + thingList + otherPara + '}" \\';
      // var  `
      // -d \'{"thingList": ["` + devices[0].globalThingID + '"]}\' \\';
      // var body
      var request_url = api.newRequestAddress;
      curl = curl + apiKeyHeader + authorizationHeader + contentTypeHeader + body + `
      "` + request_url + '"';
    }

    function findVersionIndex(element) {
      return element.versionID == req.query.versionID;
    }

    var selectedIndex = results[0].versionList.findIndex(findVersionIndex);

    _.extend(pageContents, {
      selectedIndex: selectedIndex,
      apiKey: req.session.api_key,
      api: results[0],
      mainTag: results[1],
      subTag: results[2],
      apiTags: results[3],
      exampleCodes: results[4],
      exampleResponses: results[5],
      tags: results[6],
      mainTagId: req.params.mainTag,
      subTagId: req.params.subTag,
      devices: devices,
      curl: curl,
      userLoggedIn: userLoggedIn,
      beehiveUser: beehiveUser,
      devicesFunctional: devicesFunctional,
      correspondingDevices: correspondingDevices,
      tagName: {
        "cn": "tagName",
        "eg": "englishName"
      },
      category: kiiLan.translate('category'),
      update_time: kiiLan.translate('update_time'),
      service_description: kiiLan.translate('service_description'),
      basic_information: kiiLan.translate('basic_information'),
      request_url: kiiLan.translate('request_url'),
      request_parameter: kiiLan.translate('request_parameter'),
      parameter_name: kiiLan.translate('parameter_name'),
      type: kiiLan.translate('type'),
      required: kiiLan.translate('required'),
      description: kiiLan.translate('description'),
      default_value: kiiLan.translate('default_value'),
      example_code: kiiLan.translate('example_code'),
      send_request: kiiLan.translate('send_request'),
      response: kiiLan.translate('response'),
      error_code: kiiLan.translate('error_code'),
      error_response: kiiLan.translate('error_response'),
      request_expired: kiiLan.translate('request_expired'),
      current_version: kiiLan.translate('current_version')
    })
    res.render('apiContent/apiContent', pageContents);
  }).catch((error) => {
    console.log('api content error', error);
    res.render('apiContent/apiContent', pageContents);
  });
});

router.get('/:l/apiTest', function (req, res, next) {
  let $defers = [];
  try {
    let $defer1 = TSApi.getByID(req.query.apiID);
    let $defer2 = TSVisualMachine.getDevices(req.session.beehive_token);
    $defers = [$defer1, $defer2];
  } catch (e) {
    console.log(e.stack);
  };

  Promise.all($defers).then((results) => {
    var api = results[0];
    var devices = [];
    if (results[1].code == 2) {

    } else {
      results[1].devices.forEach(function (device) {
        if (device.type == results[0].device) {
          devices.push(device);
        }
      })
    }
    console.log('devices', devices);

    var pageContents = req.pageContents;
    _.extend(pageContents, {
      apiKey: req.session.api_key[0] ? req.session.api_key[0].apiKey : '',
      beehiveToken: req.session.beehive_token,
      devices: devices,
      api: results[0],
    })
    res.render('apiTest/apiTest', pageContents);
  })
})

router.get('/:l/websocketContent', function (req, res, next) {
  let $defers = [];
  try {
    let $defer1 = TSTag.findMainTags();
    $defers = [$defer1];
  } catch (errorCode) {
    console.log("error", errorCode);
  }
  Promise.all($defers).then((results) => {
    var pageContents = req.pageContents;
    _.extend(pageContents, {
      tags: results[0],
      category: kiiLan.translate('category'),
      update_time: kiiLan.translate('update_time'),
      service_description: kiiLan.translate('service_description'),
      basic_information: kiiLan.translate('basic_information'),
      request_url: kiiLan.translate('request_url'),
      request_parameter: kiiLan.translate('request_parameter'),
      parameter_name: kiiLan.translate('parameter_name'),
      type: kiiLan.translate('type'),
      required: kiiLan.translate('required'),
      description: kiiLan.translate('description'),
      default_value: kiiLan.translate('default_value'),
      message_protocal: kiiLan.translate('message_protocal'),
      subscribe_address: kiiLan.translate('subscribe_address'),
      cancel_subscribe: kiiLan.translate('cancel_subscribe'),

    })
    res.render('websocketContent/websocketContent', pageContents);
  }).catch((error) => {
    console.log(e.stack);
    res.render('websocketContent/websocketContent', pageContents);
  });
})

router.get('/:l/apiPreview', function (req, res, next) {
  let $defers = [];
  try {
    let $defer1 = TSApi.getByID(req.query.apiID);
    let $defer2 = TSApiExampleCode.getByApiID(req.query.apiID);
    let $defer3 = TSApiExampleCode.getResponseByApiID(req.query.apiID);
    $defers = [$defer1, $defer2, $defer3];
  } catch (errorCode) {
    console.log("error", errorCode);
  }

  Promise.all($defers).then((results) => {
    var pageContents = req.pageContents;
    _.extend(pageContents, {
      api: results[0],
      exampleCodes: results[1],
      exampleResponses: results[2],
      update_time: kiiLan.translate('update_time'),
      service_description: kiiLan.translate('service_description'),
      basic_information: kiiLan.translate('basic_information'),
      request_url: kiiLan.translate('request_url'),
      request_parameter: kiiLan.translate('request_parameter'),
      parameter_name: kiiLan.translate('parameter_name'),
      type: kiiLan.translate('type'),
      required: kiiLan.translate('required'),
      description: kiiLan.translate('description'),
      default_value: kiiLan.translate('default_value'),
      example_code: kiiLan.translate('example_code'),
      send_request: kiiLan.translate('send_request'),
      response: kiiLan.translate('response'),
      error_code: kiiLan.translate('error_code'),
      error_response: kiiLan.translate('error_response'),
      request_expired: kiiLan.translate('request_expired'),
    })
    res.render('apiPreview/apiPreview', pageContents);
  }).catch((error) => {
    console.log(e.stack);
    res.render('apiPreview/apiPreview', pageContents);
  });
});

router.get('/:l/files', function (req, res, next) {
  var pageContents = req.pageContents;
  var files = [{
    title: '行业模板',
    link: '/assets/files/行业模板.pdf',
    time: '2016年12月',
    size: '182 KB',
    format: 'pdf'
  }, {
    title: '快速入门',
    link: '/assets/files/快速入门.pdf',
    time: '2016年12月',
    size: '1.3 MB',
    format: 'pdf'
  }, {
    title: '设备模拟器',
    link: '/assets/files/设备模拟器.zip',
    time: '2016年12月',
    size: '766 KB',
    format: 'zip'
  }]
  _.extend(pageContents, {
    files: files,
  })
  res.render('files/files', pageContents);
})

router.get('/:l/personal', function (req, res, next) {
  var pageContents = req.pageContents;
  _.extend(pageContents, {
    apiKey: req.session.api_key[0].apiKey,
    user: req.session.user
  });
  res.render('personal/personal', pageContents);
});

var init = function (app) {
  app.use('/', router);

  let routers = routeConfig.routers;
  _.each(routers, router => useRoute(router, app));
};

function useRoute(router, app) {
  let path = router.path;
  let module = router.router;
  app.use(path, module);
}

module.exports = init;