'use strict';

var _ = require('underscore');
var expect = require('chai').expect;
var TSTag = require('../models/TSTag');

describe('ThirdPartyApi storage', (done) => {

  var obj1 = {
    name: 'Sensor',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod'
      + 'tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,'
      + 'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo'
  };

  var obj2 = {
    name: 'Trigger',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod'
      + 'tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,'
      + 'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo'
  };
  var obj3 = {
    name: 'Equip',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod'
      + 'tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,'
      + 'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo'
  };

  var subObj1 = {
    name: 'Light Sensor',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod'
      + 'tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,'
      + 'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo',
    upper_level: 351
  };

  var subObj2 = {
    name: 'Distance Sensor',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod'
      + 'tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,'
      + 'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo',
      upper_level: 351
  };

  var subObj3 = {
    name: 'Face Detector',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod'
      + 'tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,'
      + 'quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo',
      upper_level: 351
  };

  let objs = [obj1, obj2, obj3];
  let subObjes = [subObj1, subObj2, subObj3];
  /*
  it('create', function(done){
    let allQuery = [];
    var allObjs = objs.concat(subObjes);
    _.each(allObjs, tag=>{allQuery.push(TSTag.findOne({name: tag.name}))});

    Promise.all(allQuery).then(function(result){
      expect(result.length).to.equal(6);

      let allQuery = [];

      _.each(result, (tag, index)=>{
        if(!tag){
          allQuery.push(TSTag.create(allObjs[index]));
        }else{
          allQuery.push(TSTag.save(tag));
        }
      });

      Promise.all(allQuery).then((tags)=>{
        let allQuery = [];
        console.log('allQuery length: ' +  allQuery.length);

        _.each(tags, (tag, index)=>{
          if(index>2){
            tag.upper_level = tags[0].id;
            allQuery.push(tag.save());
          }
        });

        console.log('allQuery length: ' +  allQuery.length);

        Promise.all(allQuery).then(result=>{
          done();
        });
      });
    });
  });
  */
 
  

});

/*
tag_id: {
    type: DataTypes.INTEGER(11),
    allowNull: false,
    primaryKey: true,
    autoIncrement: true
  },
  tag_name: {
    type: DataTypes.STRING,
    allowNull: true
  },
  description: {
    type: DataTypes.STRING,
    allowNull: true
  },
  upper_level: {
    type: DataTypes.INTEGER(11),
    allowNull: true
  },
  createdBy: {
    type: DataTypes.STRING,
    allowNull: true
  },
  createdAt: {
    type: DataTypes.DATE,
    allowNull: true
  },
  updatedBy: {
    type: DataTypes.STRING,
    allowNull: true
  },
  updatedAt: {
    type: DataTypes.DATE,
    allowNull: true
  }
*/