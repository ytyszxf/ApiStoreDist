var _ = require('underscore');
var expect = require('chai').expect;
var TSApi = require('../models/TSApi');
var TSTag = require('../models/TSTag');

describe('ThirdPartyApi storage', (done) => {

  var obj = {
    api_id: 12312,
    api_name: 'api1',
    description: '',
    request_address: '',
    request_method: '',
    vendor: 'aaa',
    device: 'ddd',
    feature: 'fff'
  };
  
  it('delete', function(done){
    TSApi.create(obj).then(function (api) {
      TSApi.findOne({api_id: TSApi['api_id']}).then(function(api){
        expect(api['api_id']).to.equal(12312);
        api.destroy().then((result)=>{
          done();
        });
      });
    });
  });

  it('get api by tagid', function(done){
    TSApi.getByTagId(351).then(result=>{
      console.log(result);
      done();
    });
  });
});
